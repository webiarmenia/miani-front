import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ResetPasswordComponent} from '@app/partials/reset-password/reset-password.component';
import {ConfirmUserComponent} from '@app/partials/confirm-user/confirm-user.component';
import {NotFoundComponent} from '@app/pages/home/not-found/not-found.component';
import {UnsubscribeComponent} from '@app/partials/unsubscribe/unsubscribe.component';

const routes: Routes = [
    {path: '', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)},
    {path: 'reset-password', component: ResetPasswordComponent},
    {path: 'reset-password/:token', component: ResetPasswordComponent},
    {path: 'confirm-password/:email/:token', component: ConfirmUserComponent},
    {path: 'unsubscribe/:email', component: UnsubscribeComponent},
    {path: '**', component: NotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
