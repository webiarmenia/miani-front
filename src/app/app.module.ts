import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AlertService} from '@shared/services';
import {AlertComponent, LoadingComponent} from './shared/alert&loading';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HammerGestureConfig, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {InterceptorService} from '@shared/services/interceptor.service';
import {ResetPasswordComponent} from './partials/reset-password/reset-password.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ConfirmUserComponent} from './partials/confirm-user/confirm-user.component';
import {
    SocialLoginModule,
    AuthServiceConfig,
    FacebookLoginProvider,
    GoogleLoginProvider, VkontakteLoginProvider,
} from 'angular-6-social-login-v2';
import {NotFoundComponent} from '@app/pages/home/not-found/not-found.component';
import {UnsubscribeComponent} from './partials/unsubscribe/unsubscribe.component';

export function getAuthServiceConfigs() {
    return new AuthServiceConfig(
        [
            {
                id: FacebookLoginProvider.PROVIDER_ID,
                provider: new FacebookLoginProvider('675238336326935')
            },
            {
                id: GoogleLoginProvider.PROVIDER_ID,
                provider: new GoogleLoginProvider('AIzaSyCgwER1NnT3DpH7xhruaIMxl2Y0oYWg3Vs')
            },
            // {
            //     id: VkontakteLoginProvider.PROVIDER_ID,
            //     provider: new VkontakteLoginProvider('7069235')
            // }
        ]
    );
}

export class CustomHammerConfig extends HammerGestureConfig {
    overrides = {};
}


@NgModule({
    declarations: [
        AppComponent,
        AlertComponent,
        LoadingComponent,
        ResetPasswordComponent,
        ConfirmUserComponent,
        NotFoundComponent,
        UnsubscribeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        SocialLoginModule,
        ReactiveFormsModule,
    ],
    providers: [AlertService,
        {provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig},
        {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs},
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptorService,
            multi: true
        }

    ],
    exports: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}

