import {Component, OnDestroy, OnInit} from '@angular/core';
import {Cart} from '@shared/models/cart/Cart';

import {CartListService, ColorService, MainAuthService} from '@shared/services';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Colorr} from '@shared/models/Colorr';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {
    totalPrice = 0;
    cartVariants: Cart[] = [];
    colors: Colorr[];

    // // // Subscriptions // // //
    cartListSubscription: Subscription;
    totalPriceSubscription: Subscription;
    loginSubscription: Subscription;

    constructor(
        private colorService: ColorService,
        private mainAuthService: MainAuthService,
        private cartListService: CartListService,
        private router: Router
    ) {
        this.colors = this.colorService.colors;
        this.initProducts();
    }

    ngOnInit() {
        this.cartVariants = this.getVariants();
        // console.log('dddddddddddddddddddddddddd', this.cartVariants);
        if (this.cartVariants.length < 1) {
            this.router.navigate(['/']).then();
        }
        this.totalPrice = this.cartListService.getCartListProducts().total;
    }

    ngOnDestroy() {
        this.cartListSubscription.unsubscribe();
        this.totalPriceSubscription.unsubscribe();
        this.loginSubscription.unsubscribe();
    }

    getVariants(): Cart[] {
        return this.cartListService.getCartListProducts().items;
    }

    removeFromCart(prodDetailId) {
        if (this.cartListService.user) {
            this.cartListService.deleteCartProduct(prodDetailId, null).subscribe(
                () => {
                    if (this.cartVariants.length === 0) {
                        this.router.navigate(['/']).then();
                    }
                }
            );
        } else {
            this.cartListService.deleteCartProduct(prodDetailId, null);
            if (this.cartVariants.length === 0) {
                this.router.navigate(['/']).then();
            }
        }
    }

    initProducts() {
        this.loginSubscription = this.mainAuthService.getLoginUserInfo()
            .subscribe((user: any) => {
                if (user) {
                    this.cartVariants = user.cart.products;
                    this.totalPrice = this.cartListService.getCartsTotalPrice();
                }
            });
        this.cartListSubscription = this.cartListService.getCartListProductsSubject()
            .subscribe(customCart => {
                if (customCart) {
                    this.cartVariants = customCart.items;
                } else {
                    this.cartVariants = this.cartListService.customCart.items;
                }
            });
        this.totalPriceSubscription = this.cartListService.getCartsTotalPriceSubject()
            .subscribe(price => this.totalPrice = price);
    }
}
