import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {ColorService} from '@shared/services/color.service';
import {Product} from '@shared/models/Product';
import {ProductsService} from '@shared/services/products.service';

import {AlertService, CartListService, WishListService} from '@shared/services';
import {ActivatedRoute, Router} from '@angular/router';
import {Options} from 'ng5-slider';
import {Colorr} from '@shared/models/Colorr';
import {Category, CategoryResponse, ProductsResponse} from '@shared/models';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
    done = false;
    products: Product[];
    colors: Colorr[];
    filterColor;
    lowPrice;
    highPrice;
    currentProduct = null;
    currentVariants = [];
    currentVariant = 0;
    count = 1;
    slug: string;
    canGet = false;
    loading = false;
    selectedCategory: Category;
    currentCategory: CategoryResponse;

    @ViewChild('openColor', {static: false}) openColor: ElementRef;
    @ViewChild('openSize', {static: false}) openSize: ElementRef;
    @ViewChild('openPrice', {static: false}) openPrice: ElementRef;
    value: number;
    highValue: number;
    options: Options = {
        floor: 0,
        ceil: 300
    };

    constructor(
        private router: Router,
        private alertService: AlertService,
        private activatedRoute: ActivatedRoute,
        private colorService: ColorService,
        private productsService: ProductsService,
        private cartService: CartListService,
        private wishService: WishListService) {
        this.initCurrentCategory();
        // console.log('category');
    }

    ngOnInit() {
        this.colors = this.colorService.getColors();
        // this.products = this.productsService.getProducts();
        // this.productList();
    }

    @HostListener('document:click', ['$event']) onMouseClick(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    @HostListener('document:touch', ['$event']) touch(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    lookColor() {
        this.openColor.nativeElement.style.display = 'block';
    }

    lookSize() {
        this.openSize.nativeElement.style.display = 'block';

    }

    lookPrice() {
        this.openPrice.nativeElement.style.display = 'block';
    }
    resetFilter() {
        this.filterColor = null;
        this.initMinMax();
    }

    initVariantOrder(product, index) {
        this.currentVariant = index;
        this.currentVariants[product] = index;
    }

    addToCart(product: Product, variantId) {
        this.cartService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    buyByClick(product: Product, variantId) {
        this.cartService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
        this.router.navigate(['checkout']).then();
    }

    addToWish(product: Product, variantId) {
        this.wishService.setWishListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    navigateToProduct(slug, id) {
        this.productsService.currentCollectionName = this.currentCategory.name;
        this.productsService.currentVariant = this.currentVariant;
        this.router.navigate(['category', slug, id]).then();
    }

    initCurrentCategory() {
        this.activatedRoute.params.subscribe(params => {
            this.slug = params.slug;
            this.done = false;
            this.selectedCategory = this.productsService.categories
                .find(cat => cat.slug === params.slug);

            if (!this.selectedCategory) {
                this.productsService.categories
                    .forEach(cat => {
                        if (cat.childs) {
                            const check = cat.childs.find(catChild => catChild.slug === params.slug);
                            if (check) {
                                this.selectedCategory = check;
                            }
                        }
                    });
            }
            if (!this.selectedCategory) {
                this.productsService.categories
                    .forEach(cat => {
                        if (!cat.childs) {
                            return;
                        } else {
                            cat.childs.forEach(catChild => {
                                if (catChild.childs) {
                                    if (catChild.childs) {
                                        const check = catChild.childs.find(catChildChild => catChildChild.slug === params.slug);
                                        if (check) {
                                            this.selectedCategory = check;
                                        }
                                    }
                                }
                            });
                        }
                    });
            }
            if (!this.selectedCategory) {
                return this.router.navigate(['/']).then(er => this.alertService.error('error', 'Нету такой категории'));
            }
            if (this.selectedCategory.id === this.productsService.selectedCategory.id) {
                // console.log('local');
                this.initCategoryData(null, this.productsService.selectedCategory, res => {
                    this.done = true;
                });
            } else {
                // console.log('server');
                this.productsService.getCurrentCategoryProducts(this.selectedCategory.id, this.count)
                    .subscribe((data: CategoryResponse) => {
                        // console.log(data);
                        if (!data.products) {
                            this.products = null;
                            this.done = true;
                            return this.alertService.error('error', 'Нету продуктов');
                        }
                        this.initCategoryData(null, data, res => {
                            this.done = true;
                        });
                    });
            }
        });
    }

    openMore() {
        this.loading = true;
        this.count++;
        this.productsService.getCurrentCategoryProducts(this.selectedCategory.id, this.count)
            .subscribe((data: CategoryResponse) => {
                this.initCategoryData(data, null, res => {
                    this.loading = false;
                });
            });
    }

    initCategoryData(newData, data, cb) {
        if (newData) {
            this.products = this.products.concat(newData.products);
            this.productsService.selectedCategory.products = this.products;
            this.canGet = newData.canGet;
            this.productsService.selectedCategory.canGet = newData.canGet;
        } else {
            this.productsService.selectedCategory = data;
            this.currentCategory = data;
            this.canGet = data.canGet;
            this.products = data.products;
        }
        // console.log(this.products);
        this.products.forEach(pr => {
            const colorArr = [];
            pr.variants = pr.variants.filter(vr => {
                if (colorArr.indexOf(vr.color) < 0) {
                    colorArr.push(vr.color);
                    return vr;
                }
            });
        });
        this.initMinMax();
        this.productList();
        cb();
    }

    initMinMax() {
        let minPrice = 5000;
        let maxPrice = 0;
        this.currentCategory.products.forEach(prod => {
            prod.variants.forEach(variant => {
                maxPrice = variant.price > maxPrice ? variant.price : maxPrice;
                minPrice = variant.price < minPrice ? variant.price : minPrice;
            });
        });
        this.value = minPrice;
        this.highValue = maxPrice;
        this.options = {
            floor: minPrice,
            ceil: maxPrice
        };
    }

    initFilterColor(color) {
        if (this.filterColor === color) {
            return this.filterColor = null;
        } else {
            document.querySelectorAll('input[type="checkbox"]').forEach((inp: HTMLInputElement) => {
                if (!inp.id.indexOf('color-')) {
                    inp.checked = false;
                }
            });
            this.filterColor = color;
            let prodIndex = 0;
            this.products.forEach((prod) => {
                let varIndex = 0;
                for (const variant of prod.variants) {
                    if (this.colors[variant.color] === this.filterColor) {
                        this.currentVariants[prodIndex] = varIndex;
                        prodIndex++;
                    }
                    varIndex++;
                }
            });
        }
    }

    productList() {
        for (let j = 0; j < this.products.length; j++) {
            this.currentVariants[j] = 0;
        }
    }

    valueChange(e) {
        this.lowPrice = e;
    }

    highValueChange(e) {
        this.highPrice = e;
    }

}




