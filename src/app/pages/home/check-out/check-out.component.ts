import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, CartListService, ColorService, MainAuthService, ProductsService} from '@shared/services';
import {Colorr} from '@shared/models/Colorr';
import {UserInfo, UserSignUp} from '@shared/models/user';
import {CheckOutService} from '@shared/services/check-out.service';
import {CartList} from '@shared/models/cart/CartList';
import {Router} from '@angular/router';
import {CheckOut, Image} from '@shared/models';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-check-out',
    templateUrl: './check-out.component.html',
    styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit, OnDestroy {
    currentUser: UserInfo;
    currentUserSubs: Subscription;

    myForm: FormGroup;
    emailPattern = '^[a-z0-9._%+-]{2,15}@[a-z0-9.-]+\.[a-z]{2,4}$';
    checkOutList: CheckOut = {
        items: [{
            name: '',
            product_id: null,
            product_detail_id: null,
            product_detail_quantity: null,
            cart_quantity: null,
            color: '',
            size: '',
            price: null,
            images: []
        }],
        total: 0
    };
    cartList: CartList;
    colors: Colorr[];
    variants;
    totalPrice;
    open = false;

    constructor(
        private formBuilder: FormBuilder,
        private productsService: ProductsService,
        private cartListService: CartListService,
        private colorService: ColorService,
        private userAccountService: MainAuthService,
        private checkOutService: CheckOutService,
        private alertService: AlertService,
        private router: Router
    ) {
        this.currentUserSubs = this.userAccountService.getLoginUserInfo()
            .subscribe(user => {
                    this.currentUser = user;
                }
            );
    }

    ngOnInit() {
        this.colors = this.colorService.getColors();
        this.cartList = this.cartListService.getCartListProducts();
        this.totalPrice = this.cartListService.getCartListProducts().total;
        if (this.cartList.items.length === 0) {
            this.router.navigate(['/']).then();
        }
        this.cartList.items.forEach(it => it.variants
            .forEach(pr => this.checkOutList.items.push({
                name: it.name,
                product_id: pr.product_id,
                product_detail_id: pr.product_detail_id,
                product_detail_quantity: pr.product_detail_quantity,
                cart_quantity: pr.cart_quantity,
                color: pr.color,
                size: pr.size,
                price: pr.price,
                images: pr.images
            })));
        this.checkOutList.total = this.cartList.total;
        this.checkOutList.items.splice(0, 1);
        // console.log(this.checkOutList);
        this.initForm();
    }

    ngOnDestroy() {
        this.currentUserSubs.unsubscribe();
    }

    initForm() {
        this.myForm = this.formBuilder.group({
            firstName: [this.currentUser ? this.currentUser.name : null, [Validators.required]],
            lastName: [this.currentUser ? this.currentUser.surname : null, [Validators.required]],
            address: [this.currentUser ? this.currentUser.address : null, [Validators.required]],
            // city: ['', [Validators.required]],
            // zipCode: ['', [Validators.required]],
            email: [this.currentUser ? this.currentUser.email : null, [Validators.required, Validators.pattern(this.emailPattern)]],
            phone: [this.currentUser ? this.currentUser.phone : null, [Validators.required]]
        });
    }

    submit() {
        // console.log(this.myForm.value);
    }

    setCheckOut() {
        return this.checkOutService.checkOut(this.myForm.value, this.checkOutList).subscribe();
    }

    openForm() {
        if (this.myForm.valid) {
            this.open = !this.open;
        }
    }
}
