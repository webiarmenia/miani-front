import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ColorService} from '@shared/services/color.service';
import {AlertService, CartListService, ProductsService, WishListService} from '@shared/services';
import {Product} from '@shared/models/Product';
import {Options} from 'ng5-slider';
import {Collection} from '@shared/models';
import {Colorr} from '@shared/models/Colorr';

@Component({
    selector: 'app-collections',
    templateUrl: './collections.component.html',
    styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {

    count = 1;
    canGet = false;
    slug: string;
    selectedCollection: Collection;
    products: Product[];
    colors: Colorr[];
    filterColor;
    done = false;
    loading = false;
    currentVariants = [];
    value: number;
    highValue: number;
    options: Options = {
        floor: 0,
        ceil: 300
    };

    @ViewChild('openColor', {static: false}) openColor: ElementRef;
    @ViewChild('openSize', {static: false}) openSize: ElementRef;
    @ViewChild('openPrice', {static: false}) openPrice: ElementRef;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private colorService: ColorService,
        private alertService: AlertService,
        private productsService: ProductsService,
        private cartService: CartListService,
        private wishService: WishListService
    ) {
        this.initCurrant();
    }

    ngOnInit() {
        this.colors = this.colorService.getColors();
    }

    @HostListener('document:click', ['$event']) onMouseClick(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    @HostListener('document:touch', ['$event']) touch(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    navigateToProduct(id, index) {
        this.productsService.currentCollectionName = this.selectedCollection.name;
        this.productsService.currentVariant = index;
        this.router.navigate(['collection', this.selectedCollection.slug, id]).then();
    }

    lookColor() {
        this.openColor.nativeElement.style.display = 'block';
    }

    lookPrice() {
        this.openPrice.nativeElement.style.display = 'block';
    }

    initVariantOrder(product, index) {
        this.currentVariants[product] = index;
    }

    addToCart(product: Product, variantId) {
        this.cartService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    buyByClick(product: Product, variantId) {
        this.cartService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
        this.router.navigate(['checkout']).then();
    }

    addToWish(product: Product, variantId) {
        this.wishService.setWishListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    // // // GET COLLECTION DATA // // //

    initCurrant() {
        this.activatedRoute.params.subscribe(
            params => {
                this.slug = params.slug;
                const currentCol = this.productsService.collections.find(col => col.slug === params.slug);
                if (currentCol && currentCol.id !== this.productsService.selectedCollection.id) {
                    this.productsService.getCurrentCollectionProducts(currentCol.id, this.count)
                        .subscribe((data: Collection) => {
                            this.initCollectionData(null, data, res => {
                                this.done = true;
                            });
                        });
                } else if (currentCol && currentCol.id === this.productsService.selectedCollection.id) {
                    this.initCollectionData(null, this.productsService.selectedCollection, res => {
                        this.done = true;
                    });
                } else {
                    // console.log('stexicem aper');
                    // this.router.navigate(['/']).then(er => this.alertService.error('error', 'Нету такой коллекции'));
                }
            }
        );
    }

    openMore() {
        this.loading = true;
        this.count++;
        this.productsService.getCurrentCollectionProducts(this.selectedCollection.id, this.count)
            .subscribe((data: Collection) => {
                this.initCollectionData(data, null, res => {
                    this.loading = false;
                });
            });
    }

    initCollectionData(newData, data, cb) {
        if (newData) {
            this.products = this.products.concat(newData.products);
            this.canGet = newData.canGet;
            this.productsService.selectedCollection.products = this.products;
            this.productsService.selectedCollection.canGet = newData.canGet;
        } else {
            this.selectedCollection = data;
            this.productsService.selectedCollection = data;
            // console.log(this.selectedCollection);
            this.products = this.selectedCollection.products;
            this.canGet = data.canGet;
        }
        for (let j = 0; j < this.products.length; j++) {
            this.currentVariants[j] = 0;
        }
        this.initMinMax();
        this.products.forEach(pr => {
            const colorArr = [];
            pr.variants = pr.variants.filter(vr => {
                if (colorArr.indexOf(vr.color) < 0) {
                    colorArr.push(vr.color);
                    return vr;
                }
            });
        });
        cb();
    }

    valueChange(e) {
    }

    highValueChange(e) {
    }

    resetFilter() {
        this.filterColor = null;
        this.initMinMax();
    }
    initFilterColor(color) {
        if (this.filterColor === color) {
            return this.filterColor = null;
        } else {
            document.querySelectorAll('input[type="checkbox"]').forEach((inp: HTMLInputElement) => {
                if (!inp.id.indexOf('color-')) {
                    inp.checked = false;
                }
            });
            this.filterColor = color;
            let prodIndex = 0;
            this.products.forEach((prod) => {
                let varIndex = 0;
                for (const variant of prod.variants) {
                    if (this.colors[variant.color] === this.filterColor) {
                        this.currentVariants[prodIndex] = varIndex;
                        prodIndex++;
                    }
                    varIndex++;
                }
            });
        }
    }

    initMinMax() {
        let minPrice = 5000;
        let maxPrice = 0;
        this.selectedCollection.products.forEach(prod => {
            prod.variants.forEach(variant => {
                maxPrice = variant.price > maxPrice ? variant.price : maxPrice;
                minPrice = variant.price < minPrice ? variant.price : minPrice;
            });
        });
        this.value = minPrice;
        this.highValue = maxPrice;
        this.options = {
            floor: minPrice,
            ceil: maxPrice
        };
    }
}
