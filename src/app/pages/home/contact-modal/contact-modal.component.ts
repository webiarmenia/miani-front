import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormArray, FormBuilder} from '@angular/forms';
import {ContactService} from '@shared/services/contact.service';

@Component({
    selector: 'app-contact-modal',
    templateUrl: './contact-modal.component.html',
    styleUrls: ['./contact-modal.component.scss']
})
export class ContactModalComponent implements OnInit, OnDestroy {
    myForm: FormGroup;
    emailPattern = '^[a-z0-9._%+-]{5,15}@[a-z0-9.-]+\.[a-z]{2,4}$';

    constructor(private contact: ContactService, private formBuilder: FormBuilder) {

        this.myForm = formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            phone: ['', [Validators.required, Validators.pattern('^[+|0-9]{1,12}')]],
            subject: ['', [Validators.required]],
            text: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        this.onscroll();
        window.addEventListener('scroll', this.onscroll);
    }

    ngOnDestroy() {
        window.removeEventListener('scroll', this.onscroll);
    }

    submit() {
        this.contact.sendEmail(this.myForm.value).subscribe(
            data => {
                if (data) {
                    this.myForm.reset();
                }
            },
            err => console.log(err)
        );
    }

    onscroll() {
        window.scrollTo(0, 0);
    }


}
