import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FaqService} from '@shared/services/faq.service';
import {map} from 'rxjs/operators';


@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

    data: any;
    open = false;

    // @ViewChildren('element') element: QueryList<any>;

    constructor(private faqService: FaqService) {


    }

    ngOnInit() {
        this.faqService.getFaq()
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    this.data = data.Faqs;
                });
    }

    toggleAccordion(e) {
        e.classList.toggle('active');
    }
}
