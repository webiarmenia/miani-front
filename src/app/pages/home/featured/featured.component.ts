import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {CartListService, WishListService, ProductsService, ColorService} from '@shared/services';

import {Product} from '@shared/models/Product';
import {Colorr} from '@shared/models/colorr';
import {Collection} from '@shared/models/Collection';

@Component({
    selector: 'app-featured',
    templateUrl: './featured.component.html',
    styleUrls: ['./featured.component.scss']
})
export class FeaturedComponent implements OnInit, OnDestroy {
    products: Product[];
    collections: Collection[];
    currentVariants = [];
    currentProduct = null;
    currentColIndex = 0;
    currentColId;
    currentColSlug;
    currentColName;
    colors: Colorr[];

    @ViewChild('switch', {static: false}) switch: ElementRef;
    @ViewChild('case', {static: false}) case: ElementRef;
    @ViewChild('sale', {static: false}) sale: ElementRef;

    @HostListener('document:click', ['$event']) onMouseClick(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    @HostListener('document:touch', ['$event']) touch(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    constructor(
        private router: Router,
        private productsService: ProductsService,
        private cartListService: CartListService,
        private wishListService: WishListService,
        private colorService: ColorService) {
        this.colors = colorService.colors;
    }

    ngOnInit() {
        this.collections = this.productsService.collections;
        this.collections.forEach(col => {
            col.products.forEach(pr => {
                const colorArr = [];
                pr.variants = pr.variants.filter(vr => {
                    if (colorArr.indexOf(vr.color) < 0) {
                        colorArr.push(vr.color);
                        return vr;
                    }
                });
            });
        });
        for (let coll = 0; coll < this.collections.length; coll++) {
            this.currentVariants[coll] = [];
            for (let prod = 0; prod < this.collections[coll].products.length; prod++) {
                this.currentVariants[coll][prod] = 0;
            }
        }
        this.currentColSlug = this.collections[this.currentColIndex].slug;
        this.currentColId = this.collections[this.currentColIndex].id;
    }

    // visibleTopProductDropdown() {
    //     this.switch.nativeElement.classList.toggle('active');
    // }
    //
    // visibleProductDropdown(y) {
    //     this.currentProduct = this.currentProduct === y ? null : y;
    // }

    initVariantOrder(collection, product, index) {
        this.productsService.currentVariant = index;
        this.currentVariants[collection][product] = index;
    }

    changCollection(colIndex) {
        this.currentColIndex = colIndex;
        this.currentColSlug = this.collections[colIndex].slug;
        this.currentColId = this.collections[colIndex].id;
    }

    addToCart(product: Product, variantId) {
        this.cartListService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    buyByClick(product: Product, variantId) {
        this.cartListService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
        this.router.navigate(['checkout']).then();
    }

    addToWish(product: Product, variantId) {
        this.wishListService.setWishListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    showMore() {
        this.router.navigate(['/collection', this.currentColSlug]).then();
    }

    navigateToProduct(slug, id, index) {
        this.productsService.currentCollectionName = this.findCollectionName(slug);
        this.productsService.currentVariant = index;
        this.router.navigate(['collection', slug, id]).then();
    }

    ngOnDestroy(): void {
        window.scrollTo(0, 0);
    }

    findCollectionName(slug): string {
        this.currentColName = this.collections.filter(col => col.slug === slug)[0].collection_name;
        return this.currentColName;
    }
}
