import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CartComponent} from '@app/pages/home/cart/cart.component';
import {CheckOutComponent} from '@app/pages/home/check-out/check-out.component';
import {ProductComponent} from '@app/pages/home/product/product.component';
import {InitialComponent} from '@app/pages/home/initial/initial.component';
import {HomeComponent} from '@app/pages/home/home.component';
import {CategoriesComponent} from '@app/pages/home/categories/categories.component';
import {ProfileComponent} from '@app/pages/home/profile/profile.component';
import {CollectionsComponent} from '@app/pages/home/collections/collections.component';
import {SearchResultsComponent} from '@app/pages/home/search-results/search-results.component';
import {PagesComponent} from '@app/pages/home/pages/pages.component';
import {FaqComponent} from '@app/pages/home/faq/faq.component';

const CHILDREN: Routes = [
    {path: '', component: InitialComponent, pathMatch: 'full'},
    {path: 'cart', component: CartComponent},
    {path: 'checkout', component: CheckOutComponent},
    {path: 'products/:id', component: ProductComponent},
    {path: 'collection/:slug', component: CollectionsComponent},
    {path: 'collection/:slug/:id', component: ProductComponent},
    {path: 'category/:slug', component: CategoriesComponent},
    {path: 'category/:slug/:id', component: ProductComponent},
    {path: 'profile', component: ProfileComponent},
    {path: 'search-result', component: SearchResultsComponent},
    {path: 'pages/:slug', component: PagesComponent},
    {path: 'faq', component: FaqComponent},
];
const ROUTES: Routes = [
    {
        path: '', component: HomeComponent, children: CHILDREN
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})
export class HomeRoutingModule {
}
