import {Component, OnInit} from '@angular/core';
import {DataService} from '@shared/services/data.service';
import {AlertService} from '@shared/services';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    done = false;

    constructor(private dataService: DataService, private alertService: AlertService) {
        alertService.alert_loading('show');
        this.dataService.getHome()
            .subscribe(
                res => {
                    this.alertService.alert_loading('close');
                    this.done = true;
                },
                err => {
                    this.alertService.alert_loading('close');
                    this.alertService.error('error', err.message);
                    this.done = false;
                });
    }

    ngOnInit() {

    }

}
