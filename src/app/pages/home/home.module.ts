import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


import {HomeRoutingModule} from './home-routing.module';
import {FeaturedComponent} from '@app/pages/home/featured/featured.component';
import {PublicationsComponent} from '@app/pages/home/publications/publications.component';
import {MainSliderComponent} from '@app/pages/home/main-slider/main-slider.component';
import {CartComponent} from '@app/pages/home/cart/cart.component';
import {CheckOutComponent} from '@app/pages/home/check-out/check-out.component';
import {ProductComponent} from '@app/pages/home/product/product.component';
import {ContactModalComponent} from '@app/pages/home/contact-modal/contact-modal.component';
import {NgxGalleryModule} from 'ngx-gallery';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomeComponent} from './home.component';
import {InitialComponent} from './initial/initial.component';
import {HeaderComponent} from '@app/partials/header/header.component';
import {FooterComponent} from '@app/partials/footer/footer.component';
import {CategoriesComponent} from './categories/categories.component';
import {ProfileComponent} from './profile/profile.component';
import {CollectionsComponent} from './collections/collections.component';
import {ColorFilterPipe} from '@shared/pipes/color-filter.pipe';
import {ProfileModalComponent} from './profile/profile-modal/profile-modal.component';
import {Ng5SliderModule} from 'ng5-slider';
import {SearchResultsComponent} from './search-results/search-results.component';
import {PagesComponent} from './pages/pages.component';
import {FaqComponent} from './faq/faq.component';
import {HtmlSafePipe} from '@shared/pipes/html-safe.pipe';
import {ShareButtonsModule} from '@ngx-share/buttons';


@NgModule({
    declarations: [
        FeaturedComponent,
        PublicationsComponent,
        MainSliderComponent,
        CartComponent,
        CheckOutComponent,
        ProductComponent,
        ContactModalComponent,
        HomeComponent,
        InitialComponent,
        HeaderComponent,
        FooterComponent,
        CategoriesComponent,
        ProfileComponent,
        CollectionsComponent,
        ColorFilterPipe,
        ProfileModalComponent,
        SearchResultsComponent,
        PagesComponent,
        FaqComponent,
        HtmlSafePipe
    ],
    exports: [
        ContactModalComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        NgxGalleryModule,
        FormsModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        ShareButtonsModule
    ]
})
export class HomeModule {
}
