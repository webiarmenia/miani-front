import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {AlertService, SliderService} from '@shared/services';
import {ImageSlide} from '@shared/models/ImageSlide';
import {Router} from '@angular/router';


@Component({
    selector: 'app-main-slider',
    templateUrl: './main-slider.component.html',
    styleUrls: ['./main-slider.component.scss']
})
export class MainSliderComponent implements OnInit, AfterViewInit {
    @ViewChild('current', {static: false}) current: ElementRef;
    @ViewChild('forRout', {static: false}) forRout: ElementRef;
    @ViewChildren('oneImage') oneImage: QueryList<any>;
    imagesList: ImageSlide[];
    opacity = 0.6;
    currentSlide = 0;
    slides;
    sliderInterval;

    constructor(
        private sliderService: SliderService,
        private alertService: AlertService,
        private router: Router) {
        this.imagesList = this.sliderService.imageSlider;
    }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.slides = document.querySelectorAll('.imagesControl img');
        this.oneImage.forEach(one => {
                one.nativeElement.style.opacity = this.opacity;
            }
        );

        this.startSlide();
    }

// Set first img opacity
    slideItem(e) {
        this.oneImage.forEach(img => {
            img.nativeElement.style.opacity = 1;
            img.nativeElement.style.border = '1px solid black';
        });
        this.current.nativeElement.src = e.target.src;
        this.current.nativeElement.classList.add('fade-in');
        setTimeout(() => this.current.nativeElement.classList.remove('fade-in'), 500);
        e.target.style.opacity = this.opacity;
        e.target.style.border = '1px solid white';
    }

    navigateToProduct(e) {
        this.imagesList.forEach(img => {
            if (this.current.nativeElement.src.indexOf(img.src) >= 0 && img.productId) {
                this.router.navigate(['products/' + img.productId]).then();
            }
        });
    }

    startSlide() {
        this.sliderInterval = setInterval(() => {
            this.moveToNext();
        }, 3000);
    }
    stopSlid() {
        clearInterval(this.sliderInterval);
    }
    moveToNext() {
        this.currentSlide++;
        if (this.currentSlide <= this.oneImage.length - 1) {
            this.slides[this.currentSlide].click();
        } else {
            this.currentSlide = 0;
            this.slides[this.currentSlide].click();
        }
    }
}
