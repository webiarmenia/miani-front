import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
      const visual = document.getElementById('visual');
      const events = ['resize', 'load'];

      events.forEach((e) => {
          window.addEventListener(e, () => {
              const width = window.innerWidth;
              const height = window.innerHeight;
              const ratio = 45 / (width / height);
              visual.style.transform = 'translate(-50%, -50%) rotate(-' + ratio + 'deg)';
          });
      });



  }

}
