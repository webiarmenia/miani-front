import {Component, OnInit, Pipe} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';
import {Page} from '@shared/models';
import {AlertService} from '@shared/services';
import {ViewEncapsulation} from '@angular/cli/lib/config/schema';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
    selector: 'app-pages',
    templateUrl: './pages.component.html',
    styleUrls: ['./pages.component.scss'],
    // encapsulation: ViewEncapsulation.None,
})

export class PagesComponent implements OnInit {
    done = false;
    slug: string;
    page: Page;

    queryUrl;

    constructor(
        private alertService: AlertService,
        private activatedRoute: ActivatedRoute,
        private globals: AppGlobals,
        private http: HttpClient,
        private router: Router
    ) {
        this.queryUrl = globals.queryUrl;
        this.activatedRoute.params
            .subscribe(param => {
                this.slug = param.slug;
                this.initPage();
            });
    }


    ngOnInit() {
    }

    initPage() {
        this.alertService.alert_loading('show');
        this.http.get(`${this.queryUrl}get-page`, {params: {slug: this.slug}})
            .subscribe(
                (data: any) => {
                    if (data.status === 'error') {
                        this.alertService.alert_loading('close');
                        this.alertService.error('error', 'Что то пошло не так !!!');
                        this.router.navigate(['/']).then();
                    } else {
                        const x = data.page.content.toString().substring(data.page.content.toString().search('width'),
                            data.page.content.toString().search('width') + 15);
                        // console.log(data.page.content.toString().search('width'));
                        console.log(x);
                        this.page = {
                            id: data.page.id,
                            content: data.page.content,
                            cover: data.page.cover,
                            title: data.page.title
                        };
                        this.alertService.alert_loading('close');
                        this.done = true;
                        // console.log(this.page.content);
                    }
                },
                error => {
                    this.alertService.alert_loading('close');
                    this.alertService.error('error', error.statusText);
                }
            );
    }

}
