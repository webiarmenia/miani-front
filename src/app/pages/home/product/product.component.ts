import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Colorr} from '@shared/models/Colorr';
import {
    CartListService,
    WishListService,
    ProductsService,
    ContactService,
    AlertService,
    MainAuthService, ColorService
} from '@shared/services';
import {Product, Variant} from '@shared/models';
import {Reviews} from '@shared/models';
import {UserInfo} from '@shared/models/user';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
    activeTab = 0;
    activeImage = 0;
    currentUser: UserInfo;
    productPath = {
        main: '',
        slug: ''
    };

    currentProduct = null;
    selectedProduct: Product;
    currentQuantity = 1;
    recommendedProducts: Product[] = [];
    currentVariant: Variant;
    selectedProductDescription = '';
    selectedVariant;
    recProdVariants = [];

    done = false;
    colors: Colorr[];
    ratings = document.getElementsByName('stars-rating');
    permission: boolean;
    reviews: Reviews[];
    currentSize: string;
    currentColor: number;
    currentSizesArray = [];
    currentColorsArray = [];
    currentCollectionName;
    available = true;

    reviewGroup: FormGroup;

    imageSize = '_medium.';

    @ViewChild('productWrap', {static: false}) productWrap: ElementRef;
    @ViewChild('sizeCharts', {static: false}) sizeCharts: ElementRef;

    @HostListener('document:click', ['$event']) onMouseClick(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    @HostListener('document:touch', ['$event']) touch(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    constructor(private formBuilder: FormBuilder,
                private contactService: ContactService,
                private alertService: AlertService,
                private mainAuthService: MainAuthService,
                private productsService: ProductsService,
                private cartListService: CartListService,
                private wishListService: WishListService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private colorService: ColorService) {
        this.selectedVariant = this.productsService.currentVariant || 0;
        this.currentCollectionName = this.productsService.currentCollectionName;
        this.currentUser = this.mainAuthService.currentUserData || this.mainAuthService.currentUser;
        this.initCurrant();
        this.colors = colorService.getColors();
        this.reviewGroup = formBuilder.group({
            comment: ['', [Validators.required]],
            rating: [null]
        });
    }

    ngOnInit() {
        // this.recommended = this.productsService.getRecommended();
        // for (let j = 0; j < this.recommended.length; j++) {
        //     this.recProdVariants[j] = 0;
        // }
    }

    recomendedList() {
        for (let j = 0; j < this.recommendedProducts.length; j++) {
            this.recProdVariants[j] = 0;
        }
    }

    zoomed() {
        if (document.documentElement.clientWidth <= 798) {
            return;
        } else {
            this.productWrap.nativeElement.classList.toggle('product-zoomed');
            // console.log(this.productWrap.nativeElement.classList)''
            this.imageSize = this.imageSize === '_large.' ? '_medium.' : '_large.';
        }
    }

    sizeChartsOpener() {
        this.sizeCharts.nativeElement.style.display = 'block';
    }

    iconRemove() {
        this.sizeCharts.nativeElement.style.display = 'none';
    }

    initRecVariantOrder(recIndex, varIndex) {
        this.productsService.currentVariant = varIndex;
        // console.log(recIndex, varIndex);
        this.recProdVariants[recIndex] = varIndex;
    }

    imgActivator(index) {
        this.activeImage = index;
    }

    activateTab(index) {
        this.activeTab = index;
    }

    addToWish(product: Product, variantId) {
        // console.log(product, variantId);
        this.wishListService.setWishListProducts(product, variantId);
        document.querySelectorAll('product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    addToCart(product: Product, variantId) {
        if (this.currentQuantity < 1) {
            return alert('Вы ввели неверное число');
        } else {
            this.cartListService.setCartListProducts(product, variantId, this.currentQuantity);
            document.querySelectorAll('.product-dropdown').forEach(pr => {
                pr.classList.remove('active');
            });
        }
    }

    buyByClick(product: Product, variantId) {
        this.cartListService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
        this.router.navigate(['checkout']).then();
    }

    checkForAvailable(size, color) {
        for (let vrIndex = 0; vrIndex < this.selectedProduct.variants.length; vrIndex++) {
            // console.log(size, color);
            if (this.selectedProduct.variants[vrIndex].size === size
                && this.selectedProduct.variants[vrIndex].color === color
            ) {
                this.selectedVariant = vrIndex;
                this.currentVariant = this.selectedProduct.variants[this.selectedVariant];
                this.available = true;
                break;
            } else {
                this.available = false;
            }
        }
    }

    initSelectedVariantSize(size: string) {
        this.currentSize = size;
        this.checkForAvailable(size, this.currentColor);
    }

    initSelectedVariantColor(color: number) {
        this.currentColor = color;
        this.checkForAvailable(this.currentSize, color);
    }

    submitReview() {
        let ratingCheck = 5;
        this.ratings.forEach((inp: HTMLInputElement) => {
            if (inp.checked) {
                return this.reviewGroup.value.rating = ratingCheck;
            } else {
                ratingCheck--;
            }
        });

        if (!this.reviewGroup.valid || !this.reviewGroup.value.rating) {
            return false;
        }

        this.contactService.sendReview(this.selectedProduct.product_id, this.reviewGroup.value).subscribe(
            d => {
                this.contactService.getProductReviews(this.selectedProduct.product_id)
                    .subscribe(data => {
                        this.reviews = data.reviews;
                        this.reviewGroup.reset();
                    });
            }
        );
    }

    initCurrant() {
        this.activatedRoute.params.subscribe(params => {
            this.productPath = {
                main: location.pathname.split('/')[1],
                slug: params.slug
            };
            const productId = params.id;
            if (this.productsService.currentProduct) {
                if (this.productsService.currentProduct.product_id === +productId &&
                    this.productsService.currentProductVariant === this.productsService.currentProductVariants[this.selectedVariant]
                ) {
                    this.selectedProduct = this.productsService.currentProduct;
                    this.recommendedProducts = this.productsService.recommendedProducts;
                    this.selectedProduct.variants.filter(vr => {
                        if (this.currentColorsArray.indexOf(vr.color) < 0) {
                            this.currentColorsArray.push(vr.color);
                        }
                        if (this.currentSizesArray.indexOf(vr.size) < 0) {
                            this.currentSizesArray.push(vr.size);
                        }
                    });
                    this.selectedProductDescription = this.selectedProduct.description;
                    this.currentVariant = this.productsService.currentProductVariant;
                    this.currentSize = this.currentVariant.size;
                    this.currentColor = this.currentVariant.color;
                    this.reviews = this.productsService.currentProductReviews;
                    this.permission = this.productsService.currentProductPermission;
                    this.recomendedList();
                    this.done = true;
                } else {
                    this.loadProduct(productId);
                }
            } else {
                this.loadProduct(productId);
            }
        });
    }

    loadProduct(productId) {
        this.currentSizesArray = [];
        this.currentColorsArray = [];
        this.contactService.getProductReviews(productId)
            .subscribe(data => {
                this.reviews = data.reviews;
                this.permission = data.canLeaveReview;
                // // // Add to service // // //
                this.productsService.currentProductReviews = this.reviews;
                this.productsService.currentProductPermission = this.permission;
            });
        this.productsService.getCurrentProduct(productId)
            .subscribe((data: any) => {
                // console.log(data);
                this.selectedProduct = data.product;
                this.recommendedProducts = data.smiliar_products;
                this.recommendedProducts.forEach(pr => {
                    const colorArr = [];
                    pr.variants = pr.variants.filter(vr => {
                        if (colorArr.indexOf(vr.color) < 0) {
                            colorArr.push(vr.color);
                            return vr;
                        }
                    });
                });
                this.selectedProduct.variants.filter(vr => {
                    if (this.currentColorsArray.indexOf(vr.color) < 0) {
                        this.currentColorsArray.push(vr.color);
                    }
                    if (this.currentSizesArray.indexOf(vr.size) < 0) {
                        this.currentSizesArray.push(vr.size);
                    }
                });
                this.selectedProductDescription = this.selectedProduct.description;
                this.currentVariant = this.selectedProduct.variants[this.selectedVariant];
                this.currentSize = this.currentVariant.size;
                this.currentColor = this.currentVariant.color;
                this.recomendedList();
                // // // Add to service // // //
                this.productsService.currentProduct = data.product;
                this.productsService.recommendedProducts = this.recommendedProducts;
                this.productsService.currentProductVariant = this.currentVariant;
                this.productsService.currentProductVariants = this.selectedProduct.variants;
                this.done = true;
            });
    }

    ngOnDestroy(): void {
        window.scrollTo(0, 0);
        this.currentSizesArray = [];
        this.currentColorsArray = [];
        this.currentCollectionName = null;
    }
}
