import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, MainAuthService} from '@shared/services';
import {UserInfo} from '@shared/models/user';


@Component({
    selector: 'app-profile-modal',
    templateUrl: './profile-modal.component.html',
    styleUrls: ['./profile-modal.component.scss']
})
export class ProfileModalComponent implements OnInit {
    @Input('updateItem') updateItem;
    @Input('currentUser') currentUser: UserInfo;
    myForm: FormGroup;
    emailPattern = '^[a-z0-9._%+-]{5,15}@[a-z0-9.-]+\.[a-z]{2,4}$';

    constructor(private formBuilder: FormBuilder, private userService: MainAuthService, private alertService: AlertService) {
    }

    ngOnInit() {
        this.myForm = this.formBuilder.group({
            address: [this.currentUser.address, [Validators.required]],
            email: [this.currentUser.email, [Validators.required, Validators.pattern(this.emailPattern)]],
            phone: [this.currentUser.phone, [Validators.required, Validators.pattern('[0-9]{1,12}')]]
        });
    }

    submit() {
        this.userService.updateUserInfo(this.currentUser, this.myForm.value).subscribe();
    }
}
