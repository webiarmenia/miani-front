import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AlertService, MainAuthService} from '@shared/services';
import {UserInfo, UserSignUp} from '@shared/models/user';
import {Router} from '@angular/router';
import {Order} from '@shared/models';
import {OrderDetails} from '@shared/models/OrderDetails';
import {DataService} from '@shared/services/data.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
    opened = false;
    done = false;
    openProdModal = false;
    updateItem;
    currentUser: UserInfo;
    currentUserSubs: Subscription;
    orders: Order[];
    productsToShow: OrderDetails[];
    statuses = {waiting: 'Ожидание', inprocess: 'В процессе', done: 'Доставлено'};


    @ViewChild('FirstSection', {static: false}) FirstSection: ElementRef;
    @ViewChild('SecondSection', {static: false}) SecondSection: ElementRef;


    constructor(
        private userAccountService: MainAuthService,
        private dataService: DataService,
        private alertService: AlertService,
        private router: Router) {
        this.currentUser = userAccountService.currentUserData;
        this.currentUserSubs = this.userAccountService.getLoginUserInfo()
            .subscribe(user => {
                this.currentUser = user;
                this.close();
            });
    }

    ngOnInit() {
        if (!this.currentUser) {
            this.router.navigate(['/']).then();
        }
        this.getOrders();
    }

    openFirstSection() {
        this.SecondSection.nativeElement.style.display = 'none';
        this.FirstSection.nativeElement.style.display = 'block';
    }

    openSecondSection() {
        this.FirstSection.nativeElement.style.display = 'none';
        this.SecondSection.nativeElement.style.display = 'block';
    }

    openModal(item) {
        this.opened = true;
        this.updateItem = item;
    }

    close() {
        this.opened = false;
    }

    deleteUser() {
        if (confirm('Вы уверенны что хотите удалить аккаунт? ')) {
            const pass = prompt('Ваш пороль');
            this.alertService.alert_loading('show');
            this.userAccountService.deleteAccount(pass).subscribe((d: any) => {
                if (d.status === 'success') {
                    // console.log('if');
                    this.userAccountService.signOut();
                    this.alertService.success('success', 'Ваш акаунт удален !!!');
                    location.href = '/';
                } else {
                    // console.log('else');
                    this.alertService.alert_loading('close');
                    this.alertService.error('error', 'Не верный пороль !!!');
                }
            }, e => {
                this.alertService.alert_loading('close');
                this.alertService.error('error', 'Что то пошло не так');
            });
        }
    }

    toggleProductsTab(index) {
        this.productsToShow = (index || index === 0) ? this.orders[index].product_details : null;
        this.openProdModal = !this.openProdModal;
        this.openProdModal ? document.body.classList.add('no-overflow') : document.body.classList.remove('no-overflow');
    }

    getOrders() {
        this.dataService.getOrders().subscribe(
            (d: Order[]) => {
                this.orders = d;
                this.done = true;
            }
        );
    }

    ngOnDestroy(): void {
        document.body.classList.remove('no-overflow');
        this.currentUserSubs.unsubscribe();
    }
}
