import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '@shared/models';
import {ActionsService, CartListService, ColorService, ProductsService, SearchService, WishListService} from '@shared/services';
import {Colorr} from '@shared/models/Colorr';
import {Options} from 'ng5-slider';

@Component({
    selector: 'app-search-results',
    templateUrl: './search-results.component.html',
    styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
    @ViewChild('openPrice', {static: false}) openPrice: ElementRef;
    done = false;
    currentProduct = null;
    colIndex;
    currentVariants = [];
    currentProducts: Product[] = [];
    colors: Colorr[];
    filterColor;
    value: number;
    highValue: number;
    initialColor;
    options: Options = {
        floor: 0,
        ceil: 300
    };

    @HostListener('document:click', ['$event']) onMouseClick(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    @HostListener('document:touch', ['$event']) touch(e) {
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            if (!pr.contains(e.target) && e.target.tagName !== 'IMG') {
                pr.classList.remove('active');
            }
        });
    }

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private actionService: ActionsService,
        private productsService: ProductsService,
        private wishListService: WishListService,
        private cartListService: CartListService,
        private searchService: SearchService,
        private colorService: ColorService
    ) {
        this.colors = colorService.getColors();
        activatedRoute.params.subscribe(param => {
            this.initialColor = param.color;
            const search = {
                name: param.name,
                color: +param.color,
                minprice: param.minprice,
                maxprice: param.maxprice
            };
            if (param.name === '' || param.name === 'null') {
                delete search.name;
            }
            if (param.color === 'null' || param.color === 'undefined') {
                delete search.color;
            }
            if (param.minprice === '') {
                delete search.minprice;
            }
            if (param.maxprice === '') {
                delete search.maxprice;
            }
            console.log(search);
            this.searchService.searchProducts(search)
                .subscribe(data => {
                    console.log('Search result', data);
                    if (data.products) {
                        this.actionService.searchCloseSubject.next(true);
                        return this.router.navigate(['/']).then();
                    }
                    this.currentProducts = data;
                    this.currentProducts.forEach(pr => {
                        const colorArr = [];
                        if (pr) {
                            pr.variants = pr.variants.filter(vr => {
                                if (colorArr.indexOf(vr.color) < 0) {
                                    colorArr.push(vr.color);
                                    return vr;
                                }
                            });
                        }
                    });
                    // console.log('ts: ', this.currentProducts);
                    this.initMinMax();
                    this.initCurrantVariant();
                    this.actionService.searchCloseSubject.next(true);
                    this.done = true;
                });
        });
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.actionService.searchCloseSubject.next(false);
    }

    initVariantOrder(product, index) {
        this.colIndex = index;
        this.currentVariants[product] = index;
    }

    initCurrantVariant() {
        // console.log('initVariant');
        for (let j = 0; j < this.currentProducts.length; j++) {
            let col = 0;
            this.currentProducts[j].variants.forEach((variant, index) => {
                col = variant.color === this.initialColor ? index : col;
            });
            this.currentVariants[j] = col;
        }
        // console.log('init --', this.currentProducts, 'variant array', this.currentVariants);
    }

    addToCart(product: Product, variantId) {
        this.cartListService.setCartListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    buyByClick(product) {
        this.cartListService.getCartListProducts();
        // console.log(product);
    }

    addToWish(product: Product, variantId) {
        this.wishListService.setWishListProducts(product, variantId);
        document.querySelectorAll('.product-dropdown').forEach(pr => {
            pr.classList.remove('active');
        });
    }

    navigateToProduct(id, index) {
        this.productsService.currentVariant = index;
        this.router.navigate(['products/', id]).then();
    }

    valueChange(e) {
        this.afterPriceInit();
    }

    highValueChange(e) {
        this.afterPriceInit();
    }

    lookPrice() {
        this.openPrice.nativeElement.style.display = 'block';
    }

    initMinMax() {
        // console.log('minMAx start');
        let minPrice = this.currentProducts.length > 0 ? null : 0;
        let maxPrice = 0;
        this.currentProducts.forEach(prod => {
            // console.log('for 1');
            if (prod.variants) {
                prod.variants.forEach(variant => {
                    // console.log('for 2');
                    maxPrice = variant.price > maxPrice ? variant.price : maxPrice;
                    minPrice = !minPrice || variant.price < minPrice ? variant.price : minPrice;
                });
            }
        });
        // console.log('min max end');
        this.value = minPrice;
        this.highValue = maxPrice;
        this.options = {
            floor: minPrice,
            ceil: maxPrice
        };
    }

    afterPriceInit() {
        // console.log(this.value, this.highValue);
        let prodIndex = 0;
        this.currentProducts.forEach((prod) => {
            let varIndex = 0;
            for (const variant of prod.variants) {
                if (variant.price > this.value && variant.price < this.highValue) {
                    this.currentVariants[prodIndex] = varIndex;
                    prodIndex++;
                }
                varIndex++;
            }
        });
    }
}
