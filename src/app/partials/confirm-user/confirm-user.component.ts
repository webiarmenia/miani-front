import {Component, OnInit} from '@angular/core';
import {AlertService, MainAuthService} from '@shared/services';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {UserConfirm} from '@shared/models/user';

@Component({
    selector: 'app-confirm-password',
    templateUrl: './confirm-user.component.html',
    styleUrls: ['./confirm-user.component.scss']
})
export class ConfirmUserComponent implements OnInit {

    constructor(
        private http: HttpClient,
        private router: Router,
        private alertService: AlertService,
        private mainAuthService: MainAuthService,
        private activatedRoute: ActivatedRoute
    ) {
        this.alertService.alert_loading('show');
        activatedRoute.params.subscribe(
            data => {
                const userConfirm: UserConfirm = {
                    confirmed_token: data.token,
                    email: data.email
                };
                this.mainAuthService.confirmUser(userConfirm).subscribe();
            }
        );
    }

    ngOnInit() {
    }

}
