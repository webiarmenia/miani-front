import {Component, OnDestroy, OnInit} from '@angular/core';
import {ContactService, MenuService, ProductsService} from '@shared/services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Collection} from '@shared/models/Collection';
import {MenuList} from '@shared/models';
import {SettingsService} from '@shared/services/settings.service';
import {Setting} from '@shared/models/Setting';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {
    myForm: FormGroup;
    collections: Collection[];
    menuList: MenuList[] = [];
    opened = false;
    settings: Setting[];
    socialLinks: any = {};
    contactModalSubscription: Subscription;
    mobile = false;

    constructor(
        private settingService: SettingsService,
        private menuService: MenuService,
        private productsService: ProductsService,
        private formBuilder: FormBuilder,
        private contactService: ContactService,
    ) {
        this.myForm = formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
        this.menuList = menuService.headerMenu;
        this.collections = this.productsService.collections;
        this.contactModalSubscription = this.contactService.getContactModalOrder()
            .subscribe(data => {
                if (data) {
                    this.close();
                }
            });
    }

    ngOnInit() {
        this.getSocialLinks();
        if (document.documentElement.clientWidth < 798) {
            this.mobile = true;
        }
    }

    ngOnDestroy() {
        this.contactModalSubscription.unsubscribe();
    }

    submit() {
        this.contactService.subscribeUs(this.myForm.value.email).subscribe(d => {
            this.myForm.reset();
        }, e => this.myForm.reset());
    }

    open() {
        this.opened = true;
    }

    close() {
        this.opened = false;
    }

    getSocialLinks() {
        const socials = ['twitter', 'facebook', 'vkontakte', 'phone-number'];
        this.settings = this.settingService.settings;
        this.settings.forEach(set => {
            if (socials.includes(set.key) && set.value !== 'null') {
                this.socialLinks[set.key] = set.value;
            }
        });
    }
}

