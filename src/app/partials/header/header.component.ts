import {AfterViewChecked, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Options} from 'ng5-slider';
import {Category} from '@shared/models/Category';
import {
    MenuService,
    CartListService,
    MainAuthService,
    WishListService,
    SearchService,
    ProductsService,
    ContactService, AlertService, ActionsService
} from '@shared/services';
import {Wish} from '@shared/models/user/Wish';
import {ColorService} from '@shared/services/color.service';
import {DataService} from '@shared/services/data.service';
import {MenuList, Product} from '@shared/models';
import {UserInfo} from '@shared/models/user';
import {Colorr} from '@shared/models/Colorr';
import {CartList} from '@shared/models/cart/CartList';
import {SettingsService} from '@shared/services/settings.service';
import {Setting} from '@shared/models/Setting';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy, AfterViewChecked {
    logIn = true;
    showSearch = false;
    submitted = false;
    currentUser: UserInfo;
    menuList: MenuList[] = [];
    categories: Array<Category>;
    wishListProducts: Array<Wish> = [];
    mobile = false;
    openDropdown = false;

    phoneNumber: Setting;
    // // // Forms // // //
    loginForm: FormGroup;
    registerForm: FormGroup;
    formSearch: FormGroup;
    emailPattern = '^[a-z0-9._%+-]{5,30}@[a-z0-9.-]+\.[a-z]{2,7}$';
    // // // Subscribers // // //
    wishListSubscription: Subscription;
    cartListSubscription: Subscription;
    totalPriceSubscription: Subscription;
    loginSubscription: Subscription;
    contactModalSubscription: Subscription;
    searchActionSubscription: Subscription;
    // // // SearchInput // // //
    currentColor;
    currentPrice = {
        min: null,
        max: null
    };
    colors: Colorr[];
    opened = false;
    cartList: CartList;
    totalPrice = 0;
    minMaxPrice;
    value: number;
    highValue: number;
    options: Options = {
        floor: null,
        ceil: null
    };


    @ViewChild('helpDt', {static: false}) helpDt: ElementRef;
    @ViewChild('loginRegister', {static: false}) loginRegister: ElementRef;
    @ViewChild('wishListSummary', {static: false}) wishListSummary: ElementRef;
    @ViewChild('cartSummary', {static: false}) cartSummary: ElementRef;
    @ViewChild('header', {static: false}) header: ElementRef;
    @ViewChild('logoSpan', {static: false}) logoSpan: ElementRef;
    @ViewChild('productsNav', {static: false}) productsNav: ElementRef;
    @ViewChild('pageHeaderTop', {static: false}) pageHeaderTop: ElementRef;
    @ViewChild('searchForm', {static: false}) searchForm: ElementRef;
    @ViewChild('switcher', {static: false}) switcher: ElementRef;
    @ViewChild('filterColor', {static: false}) filterColor: ElementRef;
    @ViewChild('filterPrice', {static: false}) filterPrice: ElementRef;

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private actionsService: ActionsService,
        private alertService: AlertService,
        private contactService: ContactService,
        private authService: MainAuthService,
        private dataService: DataService,
        private productsService: ProductsService,
        private cartListService: CartListService,
        private searchService: SearchService,
        private colorService: ColorService,
        private wishListService: WishListService,
        private mainAuthService: MainAuthService,
        private settingService: SettingsService,
        private menuService: MenuService,
        private activatedRoute: ActivatedRoute
    ) {
        this.colors = colorService.colors;
        this.menuList = menuService.headerMenu;
        this.categories = productsService.categories;
        this.minMaxPrice = dataService.minMaxPrices;
        this.initForms();
        this.initSubscriptions();
        router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                this.closeSearch();
            }
        });
    }

    get signInForm() {
        return this.loginForm.controls;
    }

    get signUpForm() {
        return this.registerForm.controls;
    }

    ngOnInit() {
        // console.log(this.activatedRoute.url);
        // this.wishListProducts = this.wishListService.getWishListProducts();
        // this.cartList = this.cartListService.getCartListProducts();
        // this.totalPrice = this.cartList.total;
        this.options = {
            floor: this.minMaxPrice.min,
            ceil: this.minMaxPrice.max
        };
        this.currentPrice.min = this.minMaxPrice.min;
        this.currentPrice.max = this.minMaxPrice.max;
        this.value = this.minMaxPrice.min;
        this.highValue = this.minMaxPrice.max;
        this.getPhone();
    }

    ngAfterViewChecked() {
        if (window.screen.width >= 799) {
            this.header.nativeElement.classList.remove('pageHeaderMiddle');
            this.productsNav.nativeElement.classList.remove('products-navTwo');
            // this.productsNav.nativeElement.classList.add('products-nav');
            this.pageHeaderTop.nativeElement.classList.remove('pageHeaderTop');
            this.mobile = false;
        }
    }

    @HostListener('document:click', ['$event']) onMouseClick(e) {
        if (!this.header.nativeElement.contains(e.target) && e.target.tagName !== 'IMG') {
            this.closeSearch();
        }
    }

    socialLogin(site: string) {
        this.mainAuthService.socialSignIn(site);
    }

    changeLogIn() {
        this.logIn = !this.logIn;
    }

    resetPassword() {
        this.router.navigate(['/reset-password']).then();
    }

    // // // REMOVE PRODUCT // // //

    removeCartProduct(id: number) {
        if (!this.currentUser) {
            this.cartListService.deleteCartProduct(id, null);
        } else {
            this.cartListService.deleteCartProduct(id, null).subscribe();
        }
    }

    removeWishProduct(productDetailId, wishId) {
        if (!this.currentUser) {
            this.wishListService.deleteWishProduct(productDetailId, null);
        } else {
            this.wishListService.deleteWishProduct(productDetailId, wishId).subscribe();
        }
    }

    // // // ADD PRODUCT // // //

    addToCart(product: Wish) {
        this.removeWishProduct(product.product_detail_id, product.wish_id);
        this.cartListService.setCartListProductByWish(product);
    }

    // // // SIGN_IN SIGN_UP // // //

    singIn() {
        this.submitted = true;
        if (!this.loginForm.valid) {
            return false;
        } else {
            this.mainAuthService.signIn(this.loginForm.value).subscribe();
        }
    }

    singUp() {
        this.submitted = true;
        if (!this.registerForm.valid) {
            return false;
        } else {
            this.mainAuthService.signUp(this.registerForm.value)
                .subscribe(
                    data => console.log(data),
                    error => console.log(error)
                );
            // this.registerForm.reset();
        }
    }

    help(e) {
        if (e === 'close') {
            this.helpDt.nativeElement.style.display = 'none';
        } else {
            if (this.helpDt.nativeElement.contains(e.target)) {
                return;
            } else if (this.helpDt.nativeElement.style.display === 'none' && !this.helpDt.nativeElement.contains(e.target)) {
                this.helpDt.nativeElement.style.display = 'block';
            } else if (this.helpDt.nativeElement.style.display === 'block' && !this.helpDt.nativeElement.contains(e.target)) {
                this.helpDt.nativeElement.style.display = 'none';
            }
        }
    }

    login(e) {
        if (e === 'close') {
            this.loginRegister.nativeElement.style.display = 'none';
        } else {
            if (this.loginRegister.nativeElement.contains(e.target)) {
                return;
            } else if (this.loginRegister.nativeElement.style.display === 'none' && !this.loginRegister.nativeElement.contains(e.target)) {
                this.loginRegister.nativeElement.style.display = 'block';
            } else if (this.loginRegister.nativeElement.style.display === 'block' && !this.loginRegister.nativeElement.contains(e.target)) {
                this.loginRegister.nativeElement.style.display = 'none';
            }
        }
    }

    wishList(e) {
        if (e === 'close') {
            this.wishListSummary.nativeElement.style.display = 'none';
        } else {
            if (this.wishListSummary.nativeElement.contains(e.target)) {
                return;
            } else if (this.wishListSummary.nativeElement.style.display === 'none' &&
                !this.wishListSummary.nativeElement.contains(e.target)) {
                this.wishListSummary.nativeElement.style.display = 'block';
            } else if (this.wishListSummary.nativeElement.style.display === 'block' &&
                !this.wishListSummary.nativeElement.contains(e.target)) {
                this.wishListSummary.nativeElement.style.display = 'none';
            }
        }
    }

    cartSum(e) {
        if (e === 'close') {
            this.cartSummary.nativeElement.style.display = 'none';
        } else {
            if (this.cartSummary.nativeElement.contains(e.target)) {
                return;
            } else if (this.cartSummary.nativeElement.style.display === 'none' &&
                !this.cartSummary.nativeElement.contains(e.target)) {
                this.cartSummary.nativeElement.style.display = 'block';
            } else if (this.cartSummary.nativeElement.style.display === 'block' &&
                !this.cartSummary.nativeElement.contains(e.target)) {
                this.cartSummary.nativeElement.style.display = 'none';
            }
        }

    }

    openSearch() {
        this.header.nativeElement.classList.add('activeHeight');
        this.logoSpan.nativeElement.classList.add('activeHeight');
        this.productsNav.nativeElement.style.display = 'none';
        this.searchForm.nativeElement.style.display = 'block';
        this.searchForm.nativeElement.children[2].focus();
        this.switcher.nativeElement.style = 'width: 75px';
        this.showSearch = true;
    }

    closeSearch() {
        this.header.nativeElement.classList.remove('activeHeight');
        this.logoSpan.nativeElement.classList.remove('activeHeight');
        this.productsNav.nativeElement.style.display = 'block';
        this.searchForm.nativeElement.style.display = 'none';
        this.switcher.nativeElement.style = 'width: 27px';
        this.header.nativeElement.style.transition = '0.4s ease-in-out';
        this.logoSpan.nativeElement.style.transition = '0.4s ease-in-out';
        this.showSearch = false;
        this.formSearch.reset();
    }

    Color() {
        if (this.filterPrice.nativeElement.style.display === 'block') {
            this.filterPrice.nativeElement.style.display = 'none';
        }
        this.filterColor.nativeElement.style.display = 'block';
    }

    price() {
        if (this.filterColor.nativeElement.style.display === 'block') {
            this.filterColor.nativeElement.style.display = 'none';
        }
        this.filterPrice.nativeElement.style.display = 'block';
    }

    open() {
        this.opened = true;
    }

    close() {
        this.opened = false;
    }

    activeColor(e) {
        this.currentColor = e;
    }

    search() {
        const search = {
            name: this.formSearch.value.searchInput,
            color: this.currentColor,
            minprice: this.currentPrice.min,
            maxprice: this.currentPrice.max
        };
        if (this.formSearch.invalid && this.currentColor === undefined) {
            return this.actionsService.searchCloseSubject.next(true);
        } else {
            this.router.navigate(['search-result/', search]).then();
            this.closeSearch();
        }
    }

    navigateToProduct(id) {
        this.wishList('close');
        this.cartSum('close');
        this.router.navigate(['products/', id]).then();
    }

    valueChange(e) {
        this.currentPrice.min = e;
    }

    highValueChange(e) {
        this.currentPrice.max = e;
    }

    logOut() {
        return this.mainAuthService.signOut();
    }

    // // // INIT // // //

    initForms() {
        this.formSearch = this.formBuilder.group({
            searchInput: ['', [Validators.required, Validators.minLength(3)]]
        });
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            password: ['', [Validators.required, Validators.minLength(8)]]
        });
        this.registerForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            password: ['', [Validators.required, Validators.minLength(8)]],
            surname: ['', [Validators.required]],
            address: ['', [Validators.required]],
            phone: [null, [Validators.required, Validators.minLength(6)]],
            postal_code: ['']
        });
    }

    initSubscriptions() {
        this.wishListSubscription = this.wishListService.getWishListProductsSubject()
            .subscribe(products => {
                if (products) {
                    this.wishListProducts = products;
                    // console.log(products);
                } else {
                    this.wishListProducts = this.wishListService.wishListProducts;
                }
            });
        this.cartListSubscription = this.cartListService.getCartListProductsSubject()
            .subscribe(customCart => {
                // console.log(customCart.items);
                if (customCart) {
                    this.cartList.items = customCart.items;
                } else {
                    this.cartList.items = this.cartListService.customCart.items;
                }
            });
        this.totalPriceSubscription = this.cartListService.getCartsTotalPriceSubject()
            .subscribe(price => this.totalPrice = price);
        this.loginSubscription = this.mainAuthService.getLoginUserInfo()
            .subscribe((user: any) => {
                this.currentUser = user;
                if (user) {
                    this.cartList = {
                        items: user.cart.products,
                        total: this.cartListService.getCartsTotalPrice(user.cart.products)
                    };
                    this.cartListService.customCart = this.cartList;
                    this.cartListService.customCartProducts = this.cartList.items;
                    this.wishListService.wishListProducts = user.wishes;
                    this.cartListService.cartListSubject.next(this.cartList);
                    this.wishListService.wishListSubject.next(user.wishes);
                    // console.log(user.wishes);
                } else {
                    this.cartList = this.cartListService.getCartListProducts();
                    this.totalPrice = this.cartList.total;
                    this.wishListProducts = this.wishListService.wishListProducts;
                    // this.router.navigate(['/']).then(d => {
                    //     this.cartListService.customCart = this.cartList;
                    //     this.cartListService.customCartProducts = this.cartList.items;
                    //     this.wishListService.wishListProducts = [];
                    //     this.cartListService.cartListSubject.next(this.cartList);
                    //     this.wishListService.wishListSubject.next(null);
                    // });
                }
            });
        this.contactModalSubscription = this.contactService.getContactModalOrder()
            .subscribe(data => {
                if (data) {
                    this.close();
                }
            });
        this.searchActionSubscription = this.actionsService.getSearchCloseSubject()
            .subscribe(order => {
                if (order && this.header) {
                    this.currentColor = null;
                    this.options = {
                        floor: this.minMaxPrice.min,
                        ceil: this.minMaxPrice.max
                    };
                    this.currentPrice.min = this.minMaxPrice.min;
                    this.currentPrice.max = this.minMaxPrice.max;
                    this.value = this.minMaxPrice.min;
                    this.highValue = this.minMaxPrice.max;
                    this.closeSearch();
                }
            });
    }

    getPhone() {
        this.phoneNumber = this.settingService.settings.filter(set => set.key === 'phone-number')[0];
    }

    ngOnDestroy() {
        this.wishListSubscription.unsubscribe();
        this.cartListSubscription.unsubscribe();
        this.totalPriceSubscription.unsubscribe();
        this.loginSubscription.unsubscribe();
        this.contactModalSubscription.unsubscribe();
    }

    toggleChange() {
        if (window.screen.width <= 798 && this.mobile === false) {
            this.header.nativeElement.classList.add('pageHeaderMiddle');
            this.productsNav.nativeElement.classList.add('products-navTwo');
            this.pageHeaderTop.nativeElement.classList.add('pageHeaderTop');
        } else {
            this.header.nativeElement.classList.remove('pageHeaderMiddle');
            this.productsNav.nativeElement.classList.remove('products-navTwo');
            this.pageHeaderTop.nativeElement.classList.remove('pageHeaderTop');
        }
        this.mobile = !this.mobile;
    }

    closeToggle() {
        this.header.nativeElement.classList.remove('pageHeaderMiddle');
        this.productsNav.nativeElement.classList.remove('products-navTwo');
        this.pageHeaderTop.nativeElement.classList.remove('pageHeaderTop');
        this.mobile = false;
    }

    goToMainMenu() {
        this.openDropdown = !this.openDropdown;
    }

    goToDropDownMenu() {
        this.openDropdown = true;
        this.openDropdown = !this.openDropdown;
    }
}
