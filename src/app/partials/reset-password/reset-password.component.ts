import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {ResetPasswordService} from '@shared/services';
import {ActivatedRoute} from '@angular/router';
import {ServerResponse} from '@shared/models';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
    token: any;
    submitted = false;
    sendEmailForm: FormGroup;
    createNewPassword: FormGroup;
    emailPattern = '^[a-z0-9._%+-]{5,30}@[a-z0-9.-]+\.[a-z]{2,7}$';

    constructor(
        private formBuilder: FormBuilder,
        private resetPasswordService: ResetPasswordService,
        private activatedRoute: ActivatedRoute
    ) {
        this.formsInit();
        activatedRoute.params.subscribe(param => {
            if (param.token) {
                this.token = param;
            } else if (!param.token) {
                this.token = null;
            }
        });
    }

    get emailFormControl() {
        return this.sendEmailForm.controls;
    }

    get passwordFormControl() {
        return this.createNewPassword.controls;
    }

    ngOnInit() {
    }

    onSendEmail() {
        this.submitted = true;

        if (this.sendEmailForm.invalid) {
            return false;
        } else {
            this.resetPasswordService.sendEmailMessage(this.sendEmailForm.value).subscribe();
        }

    }

    onCreate() {
        this.submitted = true;

        if (this.createNewPassword.invalid) {
            return false;
        } else {
            const data = {
                password: this.createNewPassword.value.newPassword,
                confirmed_token: this.token.token
            };
            this.resetPasswordService.createNewPassword(data).subscribe();
        }
    }

    formsInit() {
        this.sendEmailForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
        });
        this.createNewPassword = this.formBuilder.group({
            newPassword: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

}
