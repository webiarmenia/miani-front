import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {ServerResponse} from '@shared/models';
import {AlertService} from '@shared/services';

@Component({
    selector: 'app-unsubscribe',
    templateUrl: './unsubscribe.component.html',
    styleUrls: ['./unsubscribe.component.scss']
})
export class UnsubscribeComponent implements OnInit {
    queryUrl;

    constructor(
        private router: Router,
        private alertService: AlertService,
        private globals: AppGlobals,
        private route: ActivatedRoute,
        private http: HttpClient
    ) {
        this.queryUrl = globals.queryUrl;
        route.params.subscribe(param => {
            this.http.post(`${this.queryUrl}unsubscribe`, {email: param.email})
                .pipe(
                    map((res: ServerResponse) => {
                        this.router.navigate(['/']).then(data => {
                            if (res.status === 'Success') {
                                this.alertService.success('success', 'Вы успешно отписались');
                            } else {
                                this.alertService.success('success', 'Вы не подписанны');
                            }
                        });
                    }),
                    catchError(err => throwError(err))
                )
                .subscribe();
        });
    }

    ngOnInit() {
    }

}
