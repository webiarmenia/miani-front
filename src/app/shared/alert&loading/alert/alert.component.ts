import { Component, OnInit, Input } from '@angular/core';

import { Note, AlertType } from '../../models/Note';
import { AlertService } from '../../services';

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})

export class AlertComponent implements OnInit {
    @Input() id: string;

    alerts: Note[] = [];

    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getAlert(this.id).subscribe((alert: Note) => {
            if (!alert.message) {
                this.alerts = [];
                return;
            }

            this.alerts.push(alert);
        });
    }

    removeAlert(alert: Note) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    cssClass(alert: Note) {
        if (!alert) {
            return;
        }

        switch (alert.type) {
            case AlertType.Success:
                return 'success';
                // return 'alert alert-success';
            case AlertType.Error:
                return 'error';
                // return 'alert alert-danger';
            case AlertType.Notice:
                return 'notice';
        }
    }
}
