import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  display = 'none';
  subscription: Subscription;

  constructor(private alertService: AlertService) {
    this.subscription = this.alertService.get_alert_loading().subscribe(display =>  this.display = display );
  }

  ngOnInit() {
  }

}
