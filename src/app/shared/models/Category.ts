export interface Category {
    id: number;
    slug: string;
    title: string;
    childs: Array<Category>;
}
