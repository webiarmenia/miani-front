import {Product} from '@shared/models/Product';

export interface CategoryResponse {
    id: number;
    name: string;
    canGet: boolean;
    slug: string;
    products: Array<Product>;
}
