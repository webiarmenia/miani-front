import {Product} from '@shared/models/Product';

export interface Collection {
    id: string;
    slug: string;
    discount: string;
    canGet: boolean;
    description: string;
    collection_name: string;
    name?: string;
    products: Product[];
}
