export interface Image {
    path: string;
    ext: string;
}
