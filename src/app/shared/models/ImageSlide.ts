export interface ImageSlide {
    src: string;
    slug: string;
    productId: string;
}
