import {MenuList} from '@shared/models/MenuList';

export class MainResponse {
    header_menus: MenuList;
}
