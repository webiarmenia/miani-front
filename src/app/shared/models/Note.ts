export class Note {
    type: AlertType;
    message: string;
    info: object;
    alertId: string;

    constructor(init?: Partial<Note>) {
        Object.assign(this, init);
    }
}

export enum AlertType {
    Success,
    Error,
    Notice
}

