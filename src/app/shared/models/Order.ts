import {OrderDetails} from '@shared/models/OrderDetails';

export interface Order {
    created_at: string;
    id: number;
    product_details: Array<OrderDetails>;
    status: string;
    total_price: number;
}
