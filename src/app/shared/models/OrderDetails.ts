export interface OrderDetails {
    product_id: string;
    product_name: string;
    color: string;
    images: {
        path: string,
        ext: string
    }[];
    price: number;
    quantity: number;
    size: string;
}
