import {Image} from '@shared/models/Image';

export class Page {
    id: number;
    title: string;
    cover: Image;
    content: string;
}
