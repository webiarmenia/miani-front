export class PageContent {
    id: number;
    cover: string;
    content: string;
    title: string;
}
