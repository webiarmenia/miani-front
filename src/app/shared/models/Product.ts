import {Variant} from '@shared/models/Variant';

export interface Product {
    description: string;
    product_id: number;
    name: string;
    variants: Array<Variant>;
}
