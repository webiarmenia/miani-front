import {Product} from '@shared/models/Product';
import {Category} from '@shared/models/Category';

export interface ProductsResponse {
    type?: string;
    id?: string;
    slug?: string;
    discount?: string;
    description?: string;
    collection_name?: string;
    category_name?: string;
    products?: Product[];
    title?: string;
    childs?: Array<Category>;
}
