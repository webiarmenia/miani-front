import {UserInfo} from '@shared/models/user';

export class ServerResponse {
    status: string;
    message: string;
    user?: UserInfo;
}
