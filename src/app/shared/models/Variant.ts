export interface Variant {
    id: string;
    available: number;
    color: number;
    size: string;
    images: [{
        path: string,
        ext: string
    }];
    price: number;
    old_price: number;
}
