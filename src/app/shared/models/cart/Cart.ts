import {CartProduct} from '@shared/models/cart/CartProduct';

export interface Cart {
    name: string;
    variants: Array<CartProduct>;
}
