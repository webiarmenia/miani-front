import {Cart} from '@shared/models';

export interface CartList {
    items: Array<Cart>;
    total: number;
}
