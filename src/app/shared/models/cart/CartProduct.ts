import {Image} from '@shared/models';

export interface CartProduct {
    product_id: number;
    product_detail_id: number;
    product_detail_quantity: number;
    cart_quantity: number;
    color: string;
    size: string;
    price: number;
    images: Array<Image>;
}
