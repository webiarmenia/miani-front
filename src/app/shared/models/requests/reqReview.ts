export interface ReqReview {
    product_id: number;
    comment: string;
    rate: number;
}
