export interface Reviews {
    comment: string;
    created_at: string;
    id: number;
    product_id: number;
    rate: number;
    updated_at: string;
    user_id: string;
    user_name: string;
}
