import {Reviews} from '@shared/models';

export interface ResReview {
    canLeaveReview: boolean;
    reviews: Array<Reviews>;
}
