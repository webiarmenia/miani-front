import {Product} from '@shared/models';
import {Wish} from '@shared/models/user/Wish';

export interface UserInfo {
    name: string;
    cart: Array<Product>;
    wishes: Array<Wish>;
    surname: string;
    email: string;
    phone: number;
    address: string;
    created: string;
    token: string;
}
