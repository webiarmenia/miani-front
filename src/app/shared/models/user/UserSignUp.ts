export interface UserSignUp {
    name: string;
    email: string;
    password: string;
    surname: string;
    address: string;
    postal_code: string;
    phone: number;
}
