import {Image} from '@shared/models';

export interface Wish {
    product_id: string;
    product_detail_id: string;
    wish_id: number;
    name: string;
    color: string;
    size: string;
    price: number;
    image: Array<Image>;
}
