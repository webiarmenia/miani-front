import {Pipe, PipeTransform} from '@angular/core';
import {Product} from '@shared/models/Product';
import {max} from 'rxjs/operators';

@Pipe({
    name: 'colorFilter'
})
export class ColorFilterPipe implements PipeTransform {

    transform(products: Product[], col: any, maxPrice: any, minPrice: any): Product[] {

        const colorIndex = col || col === 0 ? col.toString() : null;

        if (!colorIndex) {
            return products.filter(prod => {
                for (const variant of prod.variants) {
                    // console.log(variant.color, variant.price);
                    if (variant.price >= minPrice && variant.price <= maxPrice) {
                        return prod;
                    }
                }
            });
        } else {
            return products.filter(prod => {
                let isColor = false;
                for (const variant of prod.variants) {
                    if (variant.color === colorIndex) {
                        isColor = true;
                    }
                }

                for (const variant of prod.variants) {
                    // console.log(variant.color, variant.price);
                    if (variant.price >= minPrice && variant.price <= maxPrice && isColor) {
                        return prod;
                    }
                }
            });
        }

    }
}
