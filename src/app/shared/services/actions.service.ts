import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ActionsService {
    searchCloseSubject = new BehaviorSubject<boolean>(false);

    constructor() {
    }

    getSearchCloseSubject() {
        return this.searchCloseSubject.asObservable();
    }

}
