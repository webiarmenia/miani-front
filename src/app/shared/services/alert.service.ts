import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Note, AlertType } from '../models/Note';


@Injectable()
export class AlertService {
    loading: string;
    private subject = new Subject<Note>();
    private loadingSubject = new Subject<any>();

    constructor() {
    }

    alert(alert: Note) {
        this.subject.next(alert);
    }

    getAlert(alertId?: string): Observable<any> {
        return this.subject.asObservable().pipe(filter((x: Note) => x && x.alertId === alertId));
    }

    success(message: string, info: any) {
        this.alert(new Note({ message, info, type: AlertType.Success }));
        setTimeout(() => {
            this.clear(this.alert[0]);
        }, 2000);
    }

    error(message: string, info: any) {
        this.alert(new Note({ message, info, type: AlertType.Error }));
        setTimeout(() => {
            this.clear(this.alert[0]);
        }, 2000);
    }

    notice(message: string, info: any) {
        this.alert(new Note({ message, info, type: AlertType.Notice }));
        setTimeout(() => {
            this.clear(this.alert[0]);
        }, 3000);
    }

    clear(alertId?: string) {
        this.subject.next(new Note({ alertId }));
    }

    alert_loading(e: string) {
        if (e === 'show') {
            this.loading = 'block';
            this.loadingSubject.next(this.loading);
        } else if (e === 'close') {
            this.loading = 'none';
            this.loadingSubject.next(this.loading);
        }
    }

    get_alert_loading(): Observable<any>  {
        return this.loadingSubject.asObservable();
    }
}
