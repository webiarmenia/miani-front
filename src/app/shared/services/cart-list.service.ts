import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';

import {Subject, Subscription, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {MainAuthService} from '@shared/services/mainAuth.service';

import {Product} from '@shared/models/Product';
import {CartList} from '@shared/models/cart/CartList';
import {Cart} from '@shared/models';
import {AlertService} from '@shared/services/alert.service';
import {Wish} from '@shared/models/user/Wish';
import {UserInfo} from '@shared/models/user';


@Injectable({
    providedIn: 'root'
})
export class CartListService {
    queryUrl;
    customCart: CartList;
    customCartProducts: Cart[];
    cartListSubject = new Subject<any>();
    totalPriceSubject = new Subject<any>();
    // // // User // // //
    user: UserInfo;
    userSubscription: Subscription;

    constructor(
        private alertService: AlertService,
        private authService: MainAuthService,
        private globals: AppGlobals,
        private http: HttpClient
    ) {
        this.userSubscription = authService.getLoginUserInfo()
            .subscribe((user: any) => {
                if (user) {
                    this.user = user;
                    this.customCartProducts = user.cart.products;
                    this.customCart = this.getCartListProducts();
                } else {
                    this.customCart = this.getCartListProducts();
                }
            });
        this.queryUrl = globals.queryUrl;
    }

    // // // SET PRODUCTS TO CART // // //

    setCartListProductByWish(product: Wish) {
        this.customCart = this.getCartListProducts();
        let sameProduct;
        this.customCart.items.forEach(pr => {
            pr.variants.forEach(vr => {
                if (vr.product_detail_id === +product.product_id) {
                    return sameProduct = vr;
                }
            });
        });

        if (sameProduct) {
            const newProduct = {
                name: product.name,
                variants: [{
                    product_id: sameProduct.product_id,
                    product_detail_id: sameProduct.product_detail_id,
                    product_detail_quantity: sameProduct.product_detail_quantity,
                    cart_quantity: sameProduct.cart_quantity + 1,
                    color: sameProduct.color,
                    size: sameProduct.size,
                    price: sameProduct.price,
                    images: sameProduct.images,
                }]
            };
            if (!this.user) {
                this.deleteCartProduct(+product.product_id);
                this.customCart.items.push(newProduct);
                const newLocalCart = {
                    items: this.customCart.items,
                    total: this.getCartsTotalPrice(this.customCart.items)
                };
                this.cartListSubject.next(newLocalCart);
                localStorage.setItem('unLoginUserCartList', JSON.stringify(newLocalCart));
            } else {
                this.deleteCartProduct(+product.product_id, 'Dont show alert')
                    .subscribe(res => {
                        this.addNewProduct(+product.product_id, newProduct.variants[0].cart_quantity);
                    });
                this.customCartProducts.push(newProduct);

                const newCart: CartList = {
                    items: this.customCartProducts,
                    total: this.getCartsTotalPrice()
                };

                // console.log(newCart);
                this.cartListSubject.next(newCart);
            }
        } else {
            const newProduct: Cart = {
                name: product.name,
                variants: [{
                    product_id: +product.product_id,
                    product_detail_id: +product.product_detail_id,
                    product_detail_quantity: 1,
                    cart_quantity: 1,
                    color: product.color.toString(),
                    size: product.size,
                    price: product.price,
                    images: product.image,
                }]
            };
            this.customCart.items.push(newProduct);

            const newCart: CartList = {
                items: this.customCart.items,
                total: this.getCartsTotalPrice(this.customCart.items)
            };

            // console.log(newCart);
            this.cartListSubject.next(newCart);

            if (!this.user) {
                localStorage.setItem('unLoginUserCartList', JSON.stringify(newCart));
            } else {
                this.addNewProduct(product.product_detail_id, 1);
            }
        }
    }

    setCartListProducts(product: Product, variantId: number, quantity?) {
        this.customCart = this.getCartListProducts();
        let sameProduct;
        const currentProduct = product.variants.find(vr => +vr.id === variantId);
        this.customCart.items
            .map(it => {
                const check = it.variants.find(vr => vr.product_detail_id === +currentProduct.id);
                if (check) {
                    sameProduct = check;
                }
            });

        if (sameProduct) {
            const newProduct: Cart = {
                name: product.name,
                variants: [{
                    product_id: product.product_id,
                    product_detail_id: +currentProduct.id,
                    product_detail_quantity: sameProduct.product_detail_quantity,
                    cart_quantity: sameProduct.cart_quantity + 1,
                    color: currentProduct.color.toString(),
                    size: currentProduct.size,
                    price: currentProduct.price,
                    images: currentProduct.images,
                }]
            };

            if (!this.user) {
                this.deleteCartProduct(+currentProduct.id);
                this.customCart.items.push(newProduct);
                const newLocalCart = {
                    items: this.customCart.items,
                    total: this.getCartsTotalPrice(this.customCart.items)
                };
                this.cartListSubject.next(newLocalCart);
                localStorage.setItem('unLoginUserCartList', JSON.stringify(newLocalCart));
            } else {
                this.deleteCartProduct(+currentProduct.id, 'Dont show alert')
                    .subscribe(res => {
                        this.addNewProduct(currentProduct.id, newProduct.variants[0].cart_quantity);
                    });
                this.customCartProducts.push(newProduct);

                const newCart: CartList = {
                    items: this.customCartProducts,
                    total: this.getCartsTotalPrice()
                };

                this.cartListSubject.next(newCart);
            }
        } else {
            const newProduct: Cart = {
                name: product.name,
                variants: [{
                    product_id: product.product_id,
                    product_detail_id: +currentProduct.id,
                    product_detail_quantity: 1,
                    cart_quantity: quantity || 1,
                    color: currentProduct.color.toString(),
                    size: currentProduct.size,
                    price: currentProduct.price,
                    images: currentProduct.images,
                }]
            };
            this.customCart.items.push(newProduct);

            const newCart: CartList = {
                items: this.customCart.items,
                total: this.getCartsTotalPrice(this.customCart.items)
            };

            // console.log(newCart);
            this.cartListSubject.next(newCart);

            if (!this.user) {
                localStorage.setItem('unLoginUserCartList', JSON.stringify(newCart));
            } else {
                this.addNewProduct(currentProduct.id, quantity || 1);
            }
        }
    }

    // // // Adding new Product to Cart // // //

    addNewProduct(id, quantity: number) {
        this.alertService.alert_loading('show');
        return this.http.post(`${this.queryUrl}add-cart`, {product_detail_id: id, quantity})
            .pipe(
                map(data => {
                    this.alertService.alert_loading('close');
                    // this.alertService.notice('notice', 'Продукт добавлен в карзину');
                    this.getCartsTotalPrice();
                    return data;
                }),
                catchError(err => throwError(err))
            )
            .subscribe();
    }

    // // // GET PRODUCTS FROM CART  // // //

    getCartListProducts() {
        if (!this.user) {
            const locStoreCartList = JSON.parse(localStorage.getItem('unLoginUserCartList'));
            return locStoreCartList ? locStoreCartList : {
                items: [],
                total: 0
            };
        } else {
            return {
                items: this.customCartProducts,
                total: this.getCartsTotalPrice()
            };
        }
    }

    getCartListProductsSubject() {
        return this.cartListSubject.asObservable();
    }

    // // // DELETE CART // // //

    deleteCart() {
        return this.http.post(`${this.queryUrl}delete-cart`, {})
            .pipe(
                map(data => console.log(data)),
                catchError(err => throwError(err))
            );
    }

    // // // DELETE PRODUCT FROM CART // // //

    deleteCartProduct(id: number, message?) {
        this.customCart = this.getCartListProducts();
        const updatedCard = this.customCart.items
            .map(it => it.variants.filter(vr => vr.product_detail_id !== id));

        this.customCart.items.forEach((it, index) => {
            if (updatedCard[index].length === 0) {
                updatedCard.splice(index, 1);
                this.customCart.items.splice(index, 1);
            } else {
                it.variants = updatedCard[index];
            }
        });
        this.customCart.total = this.getCartsTotalPrice(this.customCart.items);

        if (!this.user) {
            this.cartListSubject.next(this.customCart);
            localStorage.setItem('unLoginUserCartList', JSON.stringify(this.customCart));
        } else {
            this.alertService.alert_loading('show');
            // console.log(id);
            return this.http.post(`${this.queryUrl}delete-cart-item`, {product_detail_id: id})
                .pipe(
                    map(data => {
                        // console.log('ffffffffffffffff', data);
                        this.alertService.alert_loading('close');
                        // this.alertService.notice('notice', 'Продукт удален');
                    }),
                    catchError(err => {
                        // console.log('errrrrrrrrrrrrrrrr', err);
                        this.alertService.alert_loading('close');
                        return throwError(err);
                    })
                );
        }
    }

    // // // TOTAL PRICE // // //

    getCartsTotalPrice(products?) {
        let totalPrice = 0;
        for (const item of products || this.customCartProducts) {
            for (const variant of item.variants) {
                totalPrice += variant.price * variant.cart_quantity;
            }
        }
        this.totalPriceSubject.next(totalPrice);
        return totalPrice;
    }

    getCartsTotalPriceSubject() {
        return this.totalPriceSubject.asObservable();
    }
}
