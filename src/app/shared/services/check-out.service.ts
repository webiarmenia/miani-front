import {Injectable} from '@angular/core';
import {AppGlobals} from '@app/app.globals';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {MainAuthService} from '@shared/services/mainAuth.service';
import {AlertService} from '@shared/services/alert.service';
import {CartListService} from '@shared/services/cart-list.service';

import {UserInfo} from '@shared/models/user/UserInfo';
import {CheckOut} from '@shared/models';


@Injectable({
    providedIn: 'root'
})
export class CheckOutService {
    queryUrl;
    imageUrl;
    cart: CheckOut;
    userInfo: UserInfo;

    constructor(
        private cartListService: CartListService,
        private router: Router,
        private globals: AppGlobals,
        private http: HttpClient,
        private authService: MainAuthService,
        private alertService: AlertService
    ) {
        this.queryUrl = globals.queryUrl;
        this.imageUrl = globals.imageUrl;
        this.userInfo = authService.userInfo;
    }

    checkOut(formValue, cart): Observable<any> {
        this.alertService.alert_loading('show');
        this.cart = cart;
        const body = {
            client_email: formValue.email,
            address: formValue.address,
            name: formValue.firstName,
            phone: formValue.phone,
            surname: formValue.lastName,
            total_price: this.cart.total,
            products: this.cart.items.map(item => {
                return {
                    product_detail_id: item.product_detail_id,
                    quantity: item.cart_quantity
                };
            })
        };
        return this.http.post(`${this.queryUrl}order-create`, body)
            .pipe(map(data => {
                    // console.log(data);
                    this.router.navigate(['/']).then(d => {
                        const cartList = {
                            items: [],
                            total: 0
                        };
                        localStorage.removeItem('unLoginUserCartList');
                        this.cartListService.cartListSubject.next(cartList);
                        this.alertService.alert_loading('close');
                        this.alertService.success('success', 'Ваш заказ отправлен');
                    });
                    return data;
                }),
                catchError(err => throwError(err)));
    }

    //     response : return this order , send email to this client_email and MIANI OWNER
}
