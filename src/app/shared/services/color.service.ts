import {Injectable} from '@angular/core';
import {Colorr} from '@shared/models/colorr';

@Injectable({
    providedIn: 'root'
})
export class ColorService {

    colors: Colorr[] = [
        {key: 'white', value: 'Белый'},
        {key: 'red', value: 'Красный'},
        {key: 'orange', value: 'Оранжевый'},
        {key: 'yellow', value: 'Желтый'},
        {key: 'green', value: 'Зеленый'},
        {key: 'cyan', value: 'Циан'},
        {key: 'blue', value: 'Синий'},
        {key: 'purple', value: 'Пурпурный'},
        {key: 'pink', value: 'Розовый'},
        {key: 'black', value: 'Черный'},
    ];

    constructor() {
    }

    getColors() {
        return this.colors;
    }


}
