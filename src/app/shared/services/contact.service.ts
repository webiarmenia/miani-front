import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {catchError, map} from 'rxjs/operators';
import {BehaviorSubject, throwError} from 'rxjs';

import {ServerResponse} from '@shared/models';
import {ReqReview} from '@shared/models/requests';
import {ResReview, Reviews} from '@shared/models/responses';
import {AppGlobals} from '@app/app.globals';
import {AlertService} from '@shared/services/alert.service';


@Injectable({
    providedIn: 'root'
})
export class ContactService {
    reviews: Array<Reviews> = null;
    permission: boolean;
    queryUrl;
    contactFormSubject = new BehaviorSubject(false);

    constructor(
        private alertService: AlertService,
        private globals: AppGlobals,
        private http: HttpClient) {
        this.queryUrl = globals.queryUrl;
    }

    getProductReviews(id) {
        return this.http.get(`${this.queryUrl}productReviews`, {params: {product_id: id}})
            .pipe(
                map((data: ResReview) => {
                    // console.log(data);
                    this.permission = data.canLeaveReview;
                    this.reviews = data.reviews;
                    return data;
                }),
                catchError(err => {
                    // console.log(err);
                    return throwError(err);
                })
            );
    }

    sendReview(id, review) {
        const reviewData: ReqReview = {
            product_id: id,
            comment: review.comment,
            rate: review.rating
        };

        this.alertService.alert_loading('show');

        return this.http.post(`${this.queryUrl}review`, reviewData)
            .pipe(
                map((res: ServerResponse) => {
                    this.alertService.alert_loading('close');
                    if (res.status === 'Success') {
                        this.alertService.success('success', 'Ваш отзыв принят');
                        return true;
                    } else {
                        this.alertService.error('error', 'Что то пошло не так \n Повтарите попытку');
                    }
                }),
                catchError(err => {
                    this.alertService.alert_loading('close');
                    // console.log(err);
                    return throwError(err);
                })
            );
    }

    sendEmail(contactData) {
        this.alertService.alert_loading('show');
        return this.http.post(`${this.queryUrl}send-contact-email`, contactData)
            .pipe(
                map((res: ServerResponse) => {
                    this.alertService.alert_loading('close');
                    if (res.message === 'Success') {
                        this.contactFormSubject.next(true);
                        this.alertService.success('success', res.message);
                        return true;
                    } else if (res.message === 'Error') {
                        return this.alertService.error('error', res.message);
                    }
                }),
                catchError(err => {
                    this.alertService.alert_loading('close');
                    this.alertService.error('error', err.message);
                    return throwError(err);
                })
            );
    }

    subscribeUs(eMail) {
        // console.log(eMail);
        this.alertService.alert_loading('show');
        return this.http.post(`${this.queryUrl}create-subscribers`, {email: eMail}).pipe(
            map((d: any) => {
                if (d.status === 'success') {
                    this.alertService.alert_loading('close');
                    this.alertService.success('success', '');
                    return true;
                }
            }),
            catchError(err => {
                this.alertService.alert_loading('close');
                this.alertService.error('error', 'что то пошло не так');
                return throwError(err);
            })
        );
    }

    getContactModalOrder() {
        return this.contactFormSubject.asObservable();
    }
}
