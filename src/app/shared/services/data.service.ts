import {Injectable} from '@angular/core';
import {AppGlobals} from '@app/app.globals';
import {HttpClient} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {Category, Collection, MenuList} from '@shared/models';
import {UserInfo} from '@shared/models/user';
import {ImageSlide} from '@shared/models/ImageSlide';
import {
    MainAuthService,
    MenuService,
    SliderService,
    ProductsService,
    WishListService, CartListService
} from '@shared/services';
import {SettingsService} from '@shared/services/settings.service';


@Injectable({
    providedIn: 'root'
})
export class DataService {
    done = false;

    headerMenu: MenuList[];
    categories: Category[];
    collections: Collection[];
    sliders: ImageSlide[];
    user: UserInfo;
    minMaxPrices = {};

    queryUrl;
    imageUrl;

    constructor(
        private settingService: SettingsService,
        private cartListService: CartListService,
        private wishListService: WishListService,
        private productsService: ProductsService,
        private sliderService: SliderService,
        private menuService: MenuService,
        private mainAuthService: MainAuthService,
        private globals: AppGlobals,
        private http: HttpClient
    ) {
        this.queryUrl = globals.queryUrl;
        this.imageUrl = globals.imageUrl;
    }

    initData(data) {
        this.minMaxPrices = {
            min: data.minMaxPrice.minPrice,
            max: data.minMaxPrice.maxPrice
        };
        this.categories = data.categories.map(
            item => {
                return {
                    id: item.id,
                    slug: item.slug,
                    title: item.title,
                    childs: item.childs
                };
            }
        );
        this.collections = data.collections.map(
            item => {
                return {
                    id: item.id,
                    slug: item.slug,
                    discount: item.discount,
                    description: item.description,
                    collection_name: item.collection_name,
                    products: item.products
                };
            }
        );
        this.headerMenu = data.header_menus.map(
            item => {
                return {
                    title: item.title,
                    slug: item.slug
                };
            }
        );
        this.settingService.settings = data.settings.map(
            item => {
                return {
                    key: item.key,
                    value: item.value
                };
            }
        );
        this.sliders = data.sliders.map(
            item => {
                return {
                    src: item.image + '_large.' + item.ext,
                    productId: item.product_id
                };
            }
        );
        this.user = data.user;
        if (this.user) {
            localStorage.setItem('user-info', JSON.stringify(this.user));
            this.mainAuthService.currentUserSubject.next(this.user);
        }
        this.productsService.collections = this.collections;
        this.productsService.categories = this.categories;
        this.sliderService.imageSlider = this.sliders;
        this.menuService.headerMenu = this.headerMenu;
        // this.mainAuthService.currentUserData = this.user;
        // this.cartListService.customCartProducts = data.user ? data.user.cart.products : [];
        // this.wishListService.wishListProducts = data.user ? data.user.wishes : [];
        this.mainAuthService.userInfo = data.user ? {
            name: data.user.name,
            cart: data.user.cart,
            wish: data.user.wishes,
            surname: data.user.surname,
            email: data.user.email,
            phone: data.user.phone,
            address: data.user.address,
            created: data.user.created,
            token: data.user.token
        } : this.clearUser();
    }

    clearUser() {
        localStorage.removeItem('user-info');
        localStorage.removeItem('user_token');
        return null;
    }

    getHome(): Observable<any> {
        return this.http.post(`${this.queryUrl}get-home`, {})
            .pipe(map(data => {
                    console.log(data);
                    this.initData(data);
                    this.done = true;
                    return 'login=true';
                }),
                catchError(err => {
                    return throwError(err);
                }));
    }

    getOrders() {
        return this.http.post(`${this.queryUrl}get-order`, {});
    }
}

// .pipe(map(data => {
//     return data['data'].map(items => {
//         return {
//             id: items._id,
//             title: {
//                 en: items.title.en,
//                 ru: items.title.ru,
//                 am: items.title.am
//             },
//             description: {
//                 en: items.description.en,
//                 ru: items.description.ru,
//                 am: items.description.am
//             },
//             content: {
//                 en: items.content.en,
//                 ru: items.content.ru,
//                 am: items.content.am
//             },
//             banner: items.banner
//         };
//     });
