import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class FaqService {
    queryUrl;


    constructor(private http: HttpClient, private globals: AppGlobals) {
        this.queryUrl = globals.queryUrl;
    }

    getFaq() {
       return this.http.get(`${this.queryUrl}get-faq`);
    }

}
