import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';

import {Observable, Subscription, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

import {AlertService, MainAuthService} from '@shared/services';

@Injectable({
    providedIn: 'root'
})
export class InterceptorService {
    tokenSubscription: Subscription;
    tokenRx = null;

    constructor(private mainAuthService: MainAuthService, private alertService: AlertService) {
        this.tokenSubscription = mainAuthService.getInterceptorToken().subscribe(token => this.tokenRx = token);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const idToken = localStorage.getItem('user_token');
        if (idToken || this.tokenRx) {
            const cloned = req.clone({
                headers: req.headers.set('token', (idToken || this.tokenRx))
                    .set('Access-Control-Allow-Origin', 'http://mianie.ru/')
            });
            // console.log('InterceptorService if:', req.headers);
            return next.handle(cloned)
                .pipe(
                    retry(1),
                    catchError((error: HttpErrorResponse) => {
                        const errorMessage = '';
                        console.log('InterceptorService error ', error);
                        this.alertService.error('error', error.message);
                        return throwError(errorMessage);
                    })
                );
        } else {
            const cloned = req.clone({
                headers: req.headers.set('Access-Control-Allow-Origin', 'http://mianie.ru/')
            });
            return next.handle(cloned);
        }
    }
}
