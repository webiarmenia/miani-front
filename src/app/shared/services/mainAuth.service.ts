import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map} from 'rxjs/operators';
import {BehaviorSubject, empty, throwError} from 'rxjs';

import {AlertService} from '@shared/services/alert.service';

import {AppGlobals} from '@app/app.globals';
import {ServerResponse} from '@shared/models';
import {UserConfirm, UserSignIn, UserSignUp} from '../models/user';

import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider,
    VkontakteLoginProvider
} from 'angular-6-social-login-v2';
import {UserInfo} from '@shared/models/user/UserInfo';


@Injectable({
    providedIn: 'root'
})
export class MainAuthService {
    queryUrl;
    currentUser = null;
    userInfo: UserInfo;
    currentUserData = null;
    currentUserSubject = new BehaviorSubject<UserInfo>(JSON.parse(localStorage.getItem('user-info')));
    tokenInterceptor = new BehaviorSubject<any>(localStorage.getItem('user_token'));


    constructor(
        private alertService: AlertService,
        private globals: AppGlobals,
        private socialAuthService: AuthService,
        private router: Router,
        private http: HttpClient) {

        this.queryUrl = globals.queryUrl;
    }

    // // // SOCIAL SIGN_IN SING_UP // // //

    socialSignIn(socialPlatform: string) {
        let socialPlatformProvider;

        if (socialPlatform === 'facebook') {
            socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        } else if (socialPlatform === 'google') {
            socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        } else if (socialPlatform === 'vk') {
            socialPlatformProvider = VkontakteLoginProvider.PROVIDER_ID;
        }
        console.log('first : ---- ', socialPlatformProvider);

        this.socialAuthService.signIn(socialPlatformProvider).then(
            data => {
                console.log('after : ---- ', data);
                const user = {
                    token: data.token,
                    firstName: data.name.split(' ')[0],
                    lastName: data.name.split(' ')[1],
                    email: data.email,
                    provider: socialPlatform,
                    id: +data.id
                };
                console.log('user : ---- ', user);
                this.socialSignInInitUser(user).subscribe(d => {
                    console.log(d);
                });
            }
        ).catch(e => console.log('eeeeerrrr', e));
    }

    private socialSignInInitUser(user) {
        console.log('before post', user);
        return this.http.post(`${this.queryUrl}user-create-facebook`, user)
            .pipe(
                map(data => {
                    console.log('social --------', data);
                }),
                catchError(err => throwError(err))
            );
    }

    // // // CUSTOM SIGN_IN SIGN_UP // // //

    signIn(user: UserSignIn) {
        this.alertService.alert_loading('show');

        return this.http.post(`${this.queryUrl}login`, user)
            .pipe(
                map((data: ServerResponse) => {
                    this.alertService.alert_loading('close');
                    if (data.status === 'Error') {
                        this.alertService.error('error', data.message);
                    } else if (data.status === 'Success') {
                        console.log(data);
                        this.alertService.success('success', 'Login success');
                        localStorage.clear();
                        localStorage.setItem('user-info', JSON.stringify(data.user));
                        localStorage.setItem('user_token', data.user.token);
                        this.currentUser = data.user;
                        this.currentUserSubject.next(data.user);
                        this.tokenInterceptor.next(data.user.token);
                    }
                }),
                catchError(err => {
                    this.alertService.alert_loading('close');
                    this.alertService.error('error', err.error.message);
                    return empty();
                })
            );
    }

    signUp(user: UserSignUp) {
        this.alertService.alert_loading('show');
        return this.http.post(`${this.queryUrl}user-create`, user)
            .pipe(
                map((data: ServerResponse) => {
                    if (data.status === 'Success') {
                        this.alertService.alert_loading('close');
                        this.alertService.success('success', 'Check your email');
                    } else if (data.status === 'Error') {
                        this.alertService.alert_loading('close');
                        this.alertService.error('error', data.message);
                    }
                }),
                catchError(err => {
                    this.alertService.alert_loading('close');
                    this.alertService.error('error', err.error.message);
                    return empty();
                })
            );
    }

    // // // CONFIRM-USER // // //

    confirmUser(userConfirm: UserConfirm) {
        return this.http.post(`${this.queryUrl}user-confirmed`, userConfirm)
            .pipe(
                map((user: UserInfo) => {
                    this.router.navigate(['/']).then(d => {
                        this.alertService.alert_loading('close');
                        if (user.email) {
                            localStorage.setItem('user-info', JSON.stringify(user));
                            localStorage.setItem('user_token', user.token);
                            this.currentUser = user;
                            this.currentUserSubject.next(user);
                            this.tokenInterceptor.next(user.token);
                            this.alertService.success('success', 'User confirmed');
                        } else {
                            this.alertService.error('error', 'Вы уже потвердили свой аккаунт');
                        }
                    });
                }),
                catchError(err => {
                    this.alertService.alert_loading('close');
                    this.alertService.error('error', 'something wrong');
                    return empty();
                })
            );
    }

    // // // GET OBSERVABLES // // //

    getInterceptorToken() {
        return this.tokenInterceptor.asObservable();
    }

    getLoginUserInfo() {
        return this.currentUserSubject.asObservable();
    }

    signOut() {
        this.currentUser = null;
        this.currentUserData = null;
        localStorage.removeItem('user-info');
        localStorage.removeItem('user_token');
        this.currentUserSubject.next(null);
        this.router.navigate(['/']).then();
        // this.authService.signOut();
    }

    updateUserInfo(user: UserInfo, body) {
        this.alertService.alert_loading('show');
        return this.http.post(`${this.queryUrl}user-update`, body)
            .pipe(
                map(data => {
                    this.alertService.alert_loading('close');
                    this.alertService.success('success', 'Профиль обновлен');
                    user.email = body.email;
                    user.phone = body.phone;
                    user.address = body.address;
                    this.currentUserData = user;
                    this.currentUserSubject.next(user);
                    return body;
                }),
                catchError(err => throwError(err))
            );
    }

    deleteAccount(pass) {
        this.alertService.alert_loading('show');
        const body = {password: pass};
        return this.http.post(`${this.queryUrl}user-delete`, body);
    }
}
