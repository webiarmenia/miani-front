import {Injectable} from '@angular/core';
import {Category} from '@shared/models/Category';
import {MenuList} from '@shared/models';

@Injectable({
    providedIn: 'root'
})
export class MenuService {
    headerMenu: MenuList[] = [];
    footerMenu = null;

    // categories: Array<Category> = [
    //     {
    //         id: 1, name: 'Men', children: [
    //             {
    //                 id: 112, name: 'clothing', children: [
    //                     {id: 12, name: 'Leather jackets', children: null},
    //                     {id: 152, name: 'Coats', children: null},
    //                     {id: 132, name: 'Shirts', children: null},
    //                     {id: 152, name: 'T-shirts / Polos', children: null},
    //                     {id: 122, name: 'Trousers', children: null},
    //                     {id: 4412, name: 'Suits', children: null},
    //                 ]
    //             },
    //             {
    //                 id: 112, name: 'denim', children: [
    //                     {id: 142, name: 'sh582irt', children: null},
    //                     {id: 152, name: 'su22its', children: null},
    //                     {id: 1232, name: 'trus285ers', children: null},
    //                     {id: 1352, name: 'ret54yr', children: null},
    //                     {id: 122, name: 'shit452rhgtrhrt', children: null},
    //                     {id: 44192, name: 'shier452t errt', children: null},
    //                 ]
    //             },
    //         ]
    //     },
    //
    //     {
    //         id: 5, name: 'Women', children: [
    //             {
    //                 id: 1312, name: 'clothing', children: [
    //                     {id: 122, name: 'shirt', children: null},
    //                     {id: 1252, name: 'suits', children: null},
    //                     {id: 1232, name: 'trusers', children: null},
    //                     {id: 1252, name: 'retyr', children: null},
    //                     {id: 1222, name: 'shitrhgtrhrt', children: null},
    //                     {id: 42412, name: 'shiert errt', children: null},
    //                 ]
    //             },
    //         ]
    //     },
    //     {
    //         id: 15, name: 'Boys', children: [
    //             {
    //                 id: 1612, name: 'clothing', children: [
    //                     {id: 1233, name: 'shirt', children: null},
    //                     {id: 1233, name: 'suits', children: null},
    //                     {id: 1233, name: 'trusers', children: null},
    //                     {id: 1233, name: 'retyr', children: null},
    //                     {id: 1233, name: 'shitrhgtrhrt', children: null},
    //                     {id: 42332, name: 'shiert errt', children: null},
    //                 ]
    //             },
    //             {
    //                 id: 1212, name: 'denim', children: [
    //                     {id: 1112, name: 'sh582irt', children: null},
    //                     {id: 1112, name: 'su22its', children: null},
    //                     {id: 11132, name: 'trus285ers', children: null},
    //                     {id: 11152, name: 'ret54yr', children: null},
    //                     {id: 1112, name: 'shit452rhgtrhrt', children: null},
    //                     {id: 411192, name: 'shier452t errt', children: null},
    //                 ]
    //             },
    //         ]
    //     },
    //     {
    //         id: 51, name: 'Girl', children: [
    //             {
    //                 id: 13312, name: 'clothing', children: [
    //                     {id: 212, name: 'shirt', children: null},
    //                     {id: 2152, name: 'suits', children: null},
    //                     {id: 2132, name: 'trusers', children: null},
    //                     {id: 2152, name: 'retyr', children: null},
    //                     {id: 2122, name: 'shitrhgtrhrt', children: null},
    //                     {id: 24412, name: 'shiert errt', children: null},
    //                 ]
    //             },
    //             {
    //                 id: 1512, name: 'denim', children: [
    //                     {id: 12302, name: 'sh582irt', children: null},
    //                     {id: 12302, name: 'su22its', children: null},
    //                     {id: 123032, name: 'trus285ers', children: null},
    //                     {id: 123052, name: 'ret54yr', children: null},
    //                     {id: 12302, name: 'shit452rhgtrhrt', children: null},
    //                     {id: 4230192, name: 'shier452t errt', children: null},
    //                 ]
    //             },
    //
    //         ]
    //     },
    //     {
    //         id: 51, name: 'Accessoris', children: [
    //             {
    //                 id: 16312, name: 'clothing', children: [
    //                     {id: 2312, name: 'shirt', children: null},
    //                     {id: 24152, name: 'suits', children: null},
    //                     {id: 25132, name: 'trusers', children: null},
    //                     {id: 26152, name: 'retyr', children: null},
    //                     {id: 27122, name: 'shitrhgtrhrt', children: null},
    //                     {id: 284412, name: 'shiert errt', children: null},
    //                 ]
    //             },
    //             {
    //                 id: 1512, name: 'denim', children: [
    //                     {id: 192302, name: 'sh582irt', children: null},
    //                     {id: 142302, name: 'su22its', children: null},
    //                     {id: 1423032, name: 'trus285ers', children: null},
    //                     {id: 1323052, name: 'ret54yr', children: null},
    //                     {id: 122302, name: 'shit452rhgtrhrt', children: null},
    //                     {id: 41230192, name: 'shier452t errt', children: null},
    //                 ]
    //             },
    //             {
    //                 id: 11132, name: 'denim', children: [
    //                     {id: 15232, name: 'sh582irt', children: null},
    //                     {id: 15232, name: 'su22its', children: null},
    //                     {id: 152332, name: 'trus285ers', children: null},
    //                     {id: 152352, name: 'ret54yr', children: null},
    //                     {id: 15232, name: 'shit452rhgtrhrt', children: null},
    //                     {id: 4523192, name: 'shier452t errt', children: null},
    //                 ]
    //             },
    //             {
    //                 id: 113992, name: 'denim', children: [
    //                     {id: 13432, name: 'sh582irt', children: null},
    //                     {id: 13432, name: 'su22its', children: null},
    //                     {id: 134332, name: 'trus285ers', children: null},
    //                     {id: 134352, name: 'ret54yr', children: null},
    //                     {id: 13432, name: 'shit452rhgtrhrt', children: null},
    //                     {id: 4343192, name: 'shier452t errt', children: null},
    //                 ]
    //             },
    //         ]
    //     },
    // ];

    constructor() {
    }

}
