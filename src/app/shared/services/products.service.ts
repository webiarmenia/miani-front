import {Injectable} from '@angular/core';
import {Category, CategoryResponse, Product, ProductsResponse, ServerResponse, Variant} from '@shared/models';
import {Collection} from '@shared/models/Collection';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {AlertService} from '@shared/services/alert.service';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    queryUrl;

    collections: Collection[] = [];
    categories: Category[] = [];

    currentCollection: ProductsResponse;
    currentCategory: ProductsResponse;

    // // // For Collection Page // // //
    selectedCollection: Collection = {
        id: null,
        canGet: false,
        slug: '',
        discount: '',
        description: '',
        collection_name: '',
        products: [],
    };

    // // // For Category Page // // //
    selectedCategory: CategoryResponse = {
        id: null,
        name: '',
        slug: '',
        products: [],
        canGet: false
    };

    // // // For Product Page // // //
    currentProduct: Product = null;
    recommendedProducts: Product[] = [];
    currentProductVariant: Variant = null;
    currentProductVariants: Variant[] = [];
    currentProductReviews = null;
    currentProductPermission: boolean;
    currentVariant = 0;
    currentCollectionName;

    constructor(
        private router: Router,
        private alertService: AlertService,
        private http: HttpClient,
        private globals: AppGlobals
    ) {
        this.queryUrl = globals.queryUrl;
    }

    getCurrentProduct(id) {
        this.alertService.alert_loading('show');
        return this.http.get(`${this.queryUrl}get-product`, {params: {id}})
            .pipe(
                map((data: ServerResponse) => {
                    this.alertService.alert_loading('close');
                    if (data.status === 'Error') {
                        this.router.navigate(['/']).then(s => this.alertService.error('error', data.message));
                    } else {
                        return data;
                    }
                }),
                catchError(err => {
                    this.alertService.alert_loading('close');
                    return throwError(err);
                })
            );
    }

    getCurrentCategoryProducts(id, count) {
        return this.http.get(`${this.queryUrl}category-products-part`, {params: {id, count}})
            .pipe(map(data => data),
                catchError(err => throwError(err)));
    }

    getCurrentCollectionProducts(id, count) {
        return this.http.get(`${this.queryUrl}collection-products-part`, {params: {id, count}})
            .pipe(map(data => data),
                catchError(err => throwError(err)));
    }

    // // // OLD // // //

    // getCurrentProducts(type, id) {
    //     this.alertService.alert_loading('show');
    //     return this.http.get(`${this.queryUrl}get-products`, {params: {type, id}})
    //         .pipe(
    //             map((data: ProductsResponse) => {
    //                 console.log(type, data);
    //                 this.alertService.alert_loading('close');
    //                 if (data.type === 'collection') {
    //                     this.currentCollection = data;
    //                     return data;
    //                 } else if (data.type === 'category') {
    //                     this.currentCategory = data;
    //                     return data;
    //                 }
    //             }),
    //             catchError(err => {
    //                 this.alertService.alert_loading('close');
    //                 console.log(err);
    //                 return throwError(err);
    //             })
    //         );
    // }
}
