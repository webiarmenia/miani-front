import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';
import {AlertService} from '@shared/services/alert.service';
import {Router} from '@angular/router';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {ServerResponse} from '@shared/models';

@Injectable({
    providedIn: 'root'
})
export class ResetPasswordService {
    queryUrl;

    constructor(
        private globals: AppGlobals,
        private alertService: AlertService,
        private router: Router,
        private http: HttpClient
    ) {
        this.queryUrl = globals.queryUrl;
    }

    sendEmailMessage(email) {
        this.alertService.alert_loading('show');
        return this.http.post(`${this.queryUrl}forgot-password`, email)
            .pipe(
                map((res: ServerResponse) => {
                    this.alertService.alert_loading('close');
                    if (res.status === 'Success') {
                        this.router.navigate(['/']).then();
                        this.alertService.success('success', res.message);
                    } else if (res.status === 'Error') {
                        this.alertService.error('error', res.message);
                    }
                }),
                catchError(err => {
                    this.alertService.error('error', err.message);
                    return throwError(err);
                })
            );
    }

    createNewPassword(data) {
        this.alertService.alert_loading('show');
        return this.http.post(`${this.queryUrl}reset-password`, data)
            .pipe(
                map((res: ServerResponse) => {
                    this.alertService.alert_loading('close');
                    if (res.status === 'Success') {
                        this.router.navigate(['/']).then();
                        this.alertService.success('success', res.message);
                    } else if (res.status === 'Error') {
                        this.alertService.error('error', res.message);
                    }
                }),
                catchError(err => {
                    this.alertService.error('error', err.message);
                    return throwError(err);
                })
            );
    }
}
