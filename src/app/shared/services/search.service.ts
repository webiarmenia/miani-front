import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService} from '@shared/services/alert.service';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    queryUrl;
    searchProduct;

    constructor(
        private alertService: AlertService,
        private http: HttpClient,
        private router: Router,
        private globals: AppGlobals
    ) {
        this.queryUrl = globals.queryUrl;
    }

    searchProducts(search) {
        // console.log('search service', search);
        this.alertService.alert_loading('show');
        return this.http.get(`${this.queryUrl}search-products`, {params: search})
            .pipe(
                map((searchData: any) => {
                    this.alertService.alert_loading('close');
                    this.searchProduct = searchData;
                    return searchData;
                }),
                catchError(err => throwError(err))
            );
    }
}
