import {Injectable} from '@angular/core';
import {Setting} from '@shared/models/Setting';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    settings: Setting[];

    constructor() {
    }
}
