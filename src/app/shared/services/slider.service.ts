import {Injectable} from '@angular/core';
import {ImageSlide} from '@shared/models/ImageSlide';

@Injectable({
    providedIn: 'root'
})
export class SliderService {
    imageSlider: ImageSlide[];

    constructor() {
    }
}
