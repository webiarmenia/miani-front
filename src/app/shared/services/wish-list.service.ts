import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '@app/app.globals';

import {Subject, Subscription, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {MainAuthService} from '@shared/services/mainAuth.service';
import {AlertService} from '@shared/services/alert.service';

import {Product} from '@shared/models/Product';
import {Wish} from '@shared/models/user/Wish';
import {UserInfo} from '@shared/models/user';


@Injectable({
    providedIn: 'root'
})

export class WishListService {
    queryUrl;

    wishListProducts: Array<Wish> = [];
    wishListSubject = new Subject<any>();

    // // // User // // //
    user: UserInfo;
    userSubscription: Subscription;

    constructor(
        private alertService: AlertService,
        private authService: MainAuthService,
        private globals: AppGlobals,
        private http: HttpClient
    ) {
        this.userSubscription = authService.getLoginUserInfo()
            .subscribe(user => {
                if (user) {
                    this.user = user;
                    this.wishListProducts = user.wishes;
                } else {
                    this.wishListProducts = this.getWishListProducts();
                }
            });
        this.queryUrl = globals.queryUrl;
    }

    setWishListProducts(product: Product, variantId: number) {
        const currentProduct = product.variants.find(vr => +vr.id === variantId);
        const sameProducts = this.wishListProducts.find(pr => pr.product_detail_id === currentProduct.id);
        let newProduct;
        if (!sameProducts) {
            newProduct = {
                product_detail_id: currentProduct.id,
                name: product.name,
                color: currentProduct.color,
                size: currentProduct.size,
                price: currentProduct.price,
                image: currentProduct.images
            };
        }

        if (!this.user && !sameProducts) {
            if (this.wishListProducts.length >= 5) {
                this.wishListProducts.splice(0, 1);
                this.wishListProducts.push(newProduct);
            } else {
                this.wishListProducts.push(newProduct);
                this.wishListSubject.next(this.wishListProducts);
            }
            localStorage.setItem('unLoginUserWishList', JSON.stringify(this.wishListProducts));
        } else if (this.user) {
            if (this.wishListProducts.length >= 5) {
                this.deleteWishProduct(+this.wishListProducts[4].product_detail_id, this.wishListProducts[4].wish_id, 'Dont show loading')
                    .subscribe(res => this.addProduct(currentProduct));
            } else {
                this.addProduct(currentProduct);
            }
        }
    }

    addProduct(currentProduct) {
        this.alertService.alert_loading('show');
        this.http.post(`${this.queryUrl}wish`, {product_detail_id: currentProduct.id})
            .pipe(
                map((data: any) => {
                    // console.log(`WihListData - `, data);
                    this.alertService.alert_loading('close');
                    if (typeof data === 'string') {
                        this.alertService.notice('notice', data);
                    } else {
                        this.wishListProducts = [];
                        this.wishListSubject.next(null);
                        this.wishListProducts = data.wishes;
                        this.wishListSubject.next(this.wishListProducts);
                        this.alertService.notice('notice', 'Продукт успешно добавлен');
                    }
                }),
                catchError(err => {
                    // console.log(err);
                    return throwError(err);
                })
            )
            .subscribe();
    }

    // // // DELETE PRODUCTS FROM WISH // // //

    deleteWishProduct(productId: number, wishId: number, order?: string) {
        const delPos = this.wishListProducts.findIndex(pr => +pr.product_detail_id === productId);
        this.wishListProducts.splice(delPos, 1);
        this.wishListSubject.next(this.wishListProducts);

        if (!this.user) {
            localStorage.setItem('unLoginUserWishList', JSON.stringify(this.wishListProducts));
        } else {
            if (!order) {
                this.alertService.alert_loading('show');
            }
            return this.http.post(`${this.queryUrl}delete-wish`, {product_id: productId, id: wishId})
                .pipe(
                    map(data => {
                        console.log(data);
                        if (!order) {
                            this.alertService.alert_loading('close');
                            this.alertService.success('success', 'Продукт удален из избранных');
                            return data;
                        }
                    }),
                    catchError(err => {
                        // console.log(err);
                        return throwError(err);
                    })
                );
        }
    }

    // // // GET WISH PRODUCTS // // //

    getWishListProducts() {
        if (!this.user) {
            const locStoreWishList = JSON.parse(localStorage.getItem('unLoginUserWishList'));
            return locStoreWishList ? locStoreWishList : [];
        } else {
            return this.wishListProducts;
        }
    }

    getWishListProductsSubject() {
        return this.wishListSubject.asObservable();
    }
}
