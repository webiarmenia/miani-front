$(document).ready(() => {

    $(document).mousedown(function (event) {
        /**/
        /* help */
        /**/
        if ($(event.target).parents('#help').length === 0 && $(event.target).attr('id') != 'help') {
            $('#help dd').fadeOut('fast');
        }


        /**/
        /* cart summary */
        /**/
        if ($(event.target).parents('#cart-summary').length === 0 && $(event.target).attr('id') != 'cart-summary') {
            $('#cart-summary dd').fadeOut('fast');
        }


        /**/
        /* wish list summary */
        /**/
        if ($(event.target).parents('#wish-list-summary').length === 0 && $(event.target).attr('id') != 'wish-list-summary') {
            $('#wish-list-summary dd').fadeOut('fast');
        }


        /**/
        /* login popup */
        /**/
        if ($(event.target).parents('#login-popup').length === 0 && $(event.target).attr('id') != 'login-popup') {
            $('#login-popup dd').fadeOut('fast');
        }


        /**/
        /* search */
        /**/
        if ($(event.target).parents('#search .filter').length === 0 && $(event.target).attr('class') != '.filter') {
            $('#search .filter dd').fadeOut('fast');
        }


        /**/
        /* catalog */
        /**/
        if ($(event.target).parents('#products-filter-price').length === 0 && $(event.target).attr('id') != '#products-filter-price') {
            $('#products-filter-price dd').fadeOut('fast');
        }
        if ($(event.target).parents('#products-filter-color').length === 0 && $(event.target).attr('id') != '#products-filter-color') {
            $('#products-filter-color dd').fadeOut('fast');
        }
        if ($(event.target).parents('#products-filter-size').length === 0 && $(event.target).attr('id') != '#products-filter-size') {
            $('#products-filter-size dd').fadeOut('fast');
        }


        /**/
        /* size-charts */
        /**/
        if ($(event.target).parents('#size-charts').length === 0 && $(event.target).attr('id') != 'size-charts') {
            $('#size-charts').fadeOut('fast');
        }


        /**/
        /* what is cvv */
        /**/
        if ($(event.target).parents('#what-is-cvv').length === 0 && $(event.target).attr('id') != 'what-is-cvv') {
            $('#what-is-cvv dd').fadeOut('fast');
        }
    });

    /**/
    /* product dropdown */
    /**/
    $(document).on('click', '.product-dropdown .switcher', function () {
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        } else {
            $('.product-dropdown.active').removeClass('active');
            $(this).parent().addClass('active');
        }

        return false;
    });


    /**/
    /* products showcase */
    /**/
    $(document).on('click', '.products-showcase ol .prev', function () {
        ol = $(this).closest('ol');
        li = $(this).closest('li');
        prev = li.prev().length ? li.prev() : ol.find('li:last-child');

        li.animate({opacity: 0, left: '100%'}, function () {
            li.hide();
        });

        prev.css('left', '-100%').css('display', 'block').animate({opacity: 1, left: '0%'});

        return false;
    });
    $(document).on('click', '.products-showcase ol .next', function () {
        ol = $(this).closest('ol');
        li = $(this).closest('li');
        next = li.next().length ? li.next() : ol.find('li:first-child');

        li.animate({opacity: 0, left: '-100%'}, function () {
            li.hide();
        });

        next.css('left', '100%').css('display', 'block').animate({opacity: 1, left: '0%'});

        return false;
    });


});
